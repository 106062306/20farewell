var player1_score = 0;
var player2_score = 0;
var player1_answered = false;
var player2_answered = false;
var destroyed = false;

var menuState = {
    ready: false,
    preload: function() {
        game.load.image('menu_background', 'assets/menu_background.jpg');
        //game.load.image('game_background', 'assets/game_background_.jpg');
        game.load.image('game_background', 'assets/game_background.jpg');
        game.load.image('start', 'assets/start.png');
        //game.load.audio('bgm', 'assets/bgm2.mp3');
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'menu_background');
        this.start = game.add.sprite(game.width/2-150, game.height/2-113, 'start');
        this.start.scale.x = 0.34;
        this.start.scale.y = 0.47;
        this.start.inputEnabled = true;
        this.start.input.useHandCursor = true;
        this.start.events.onInputDown.add(this.clickToStart, this);
    },
    update: function() {
        
    },
    clickToStart: function() {
        game.state.start('lobby');
    }
}
var round_over = {
    preload: function() {
        game.load.image('success', 'assets/success.jpg');
        game.load.image('even', 'assets/even.jpg');
        game.load.image('fail', 'assets/fail.jpg');
        game.load.image('success_title', 'assets/success_title.png');
        game.load.image('even_title', 'assets/even_title.png');
        game.load.image('fail_title', 'assets/fail_title.png');
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        if(player1_score > player2_score) {
            game.add.image(0, 0, 'success'); 
            this.background = game.add.tileSprite(0, 0, 600, 600, 'success');
            this.success = game.add.sprite(game.width/2-120, game.height/2-220, 'success_title');
            this.success.scale.x = 0.5;
            this.success.scale.y = 0.5;
            this.game.time.events.add(Phaser.Timer.SECOND * 3, this.reset, this);
        }
        else if(player1_score == player2_score) {
            game.add.image(0, 0, 'fail'); 
            this.background = game.add.tileSprite(0, 0, 600, 600, 'fail');
            this.even = game.add.sprite(game.width/2 - 70, game.height/2-220, 'even_title');
            this.even.scale.x = 0.5;
            this.even.scale.y = 0.5;
            this.game.time.events.add(Phaser.Timer.SECOND * 3, this.reset, this);
        }
        else {
            game.add.image(0, 0, 'even'); 
            this.background = game.add.tileSprite(0, 0, 600, 600, 'even');
            this.fail = game.add.sprite(game.width/2-120, game.height/2-220, 'fail_title');
            this.fail.scale.x = 0.5;
            this.fail.scale.y = 0.5;
            this.game.time.events.add(Phaser.Timer.SECOND * 3, this.reset, this);
        }
    },
    reset: function() {
        player1_score = 0;
        player2_score = 0;
        game.state.start('lobby');
    },
}
var lobby = {
    preload: function() {
        game.load.image('l1', 'assets/l1.jpg');
        game.load.image('l2', 'assets/l2.jpg');
        game.load.image('l3', 'assets/l3.jpg');
        game.load.image('l4', 'assets/l4.jpg');
        game.load.image('l5', 'assets/l5.jpg');
        game.load.image('l1_title', 'assets/l1_title.png');
        game.load.image('l2_title', 'assets/l2_title.png');
        game.load.image('l3_title', 'assets/l3_title.png');
        game.load.image('l4_title', 'assets/l4_title.png');
        game.load.image('l5_title', 'assets/l5_title.png');
        game.load.image('game_background', 'assets/game_background.jpg');
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'game_background'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'game_background');
        this.levelOne = game.add.sprite(game.width/2-145, game.height/2-197, 'l1');
        this.levelOne.scale.x = 0.33;
        this.levelOne.scale.y = 0.16;
        this.levelOne.inputEnabled = true;
        this.levelOne.input.useHandCursor = true;
        this.levelOne.events.onInputDown.add(this.clickToL1, this);
        this.levelTwo = game.add.sprite(game.width/2-145, game.height/2-105, 'l2');
        this.levelTwo.scale.x = 0.331;
        this.levelTwo.scale.y = 0.17;
        this.levelTwo.inputEnabled = true;
        this.levelTwo.input.useHandCursor = true;
        this.levelTwo.events.onInputDown.add(this.clickToL2, this);
        this.levelThree = game.add.sprite(game.width/2-145, game.height/2-7, 'l3');
        this.levelThree.scale.x = 0.33;
        this.levelThree.scale.y = 0.16;
        this.levelThree.inputEnabled = true;
        this.levelThree.input.useHandCursor = true;
        this.levelThree.events.onInputDown.add(this.clickToL3, this);
        this.levelFour = game.add.sprite(game.width/2-145, game.height/2+86, 'l4');
        this.levelFour.scale.x = 0.33;
        this.levelFour.scale.y = 0.16;
        this.levelFour.inputEnabled = true;
        this.levelFour.input.useHandCursor = true;
        this.levelFour.events.onInputDown.add(this.clickToL4, this);
        this.levelFive = game.add.sprite(game.width/2-145, game.height/2+178, 'l5');
        this.levelFive.scale.x = 0.33;
        this.levelFive.scale.y = 0.16;
        this.l1_title = game.add.sprite(game.width/2-165, game.height/2-213, 'l1_title');
        this.l1_title.scale.x = 0.4;
        this.l1_title.scale.y = 0.4;
        this.l2_title = game.add.sprite(game.width/2-165, game.height/2-119, 'l2_title');
        this.l2_title.scale.x = 0.4;
        this.l2_title.scale.y = 0.4;
        this.l3_title = game.add.sprite(game.width/2-165, game.height/2-26, 'l3_title');
        this.l3_title.scale.x = 0.4;
        this.l3_title.scale.y = 0.4;
        this.l4_title = game.add.sprite(game.width/2-165, game.height/2+67, 'l4_title');
        this.l4_title.scale.x = 0.4;
        this.l4_title.scale.y = 0.4;
        this.l5_title = game.add.sprite(game.width/2-165, game.height/2+162, 'l5_title');
        this.l5_title.scale.x = 0.4;
        this.l5_title.scale.y = 0.4;
        //this.bgm = game.sound.play('bgm');
        //this.bgm.loopFull(0.6);
        /*this.text = game.add.text(game.world.centerX-280, game.world.centerY+230, "Score: " + score, {
            font: "25px Courier New",
            fill: "#000000",
            align: "center"
        });*/
    },
    clickToL1: function() {
        game.state.start('l1_1');
    },
    clickToL2: function() {
        game.state.start('l2_1');
    },
    clickToL3: function() {
        game.state.start('l3_1');
    },
    clickToL4: function() {
        game.state.start('l4_1');
    },
    render: function() {
        game.debug.geom(this.HP, '#0fffff');
    }
};
var l1_1 = {
    preload: function() {
        game.load.image('player1_1_1', 'assets/l1_p1.jpg');
        game.load.image('player2_1_1', 'assets/l1_p2.jpg');
        game.load.image('bar_1_1', 'assets/bar.png');
        game.load.image('option_1_1', 'assets/option.png');
        game.load.image('q1_1o1', 'assets/q1_1o1.png'); //unique to each round
        game.load.image('q1_1o2', 'assets/q1_1o2.png'); //unique to each round
        game.load.image('q1_1o3', 'assets/q1_1o3.png'); //unique to each round
        game.load.image('q1_1o4', 'assets/q1_1o4.png'); //unique to each round
        game.load.image('gaming_1_1', 'assets/game_situation.jpg');
        game.load.image('q1', 'assets/question_1.png'); //unique to each round
        game.load.image('firstq', 'assets/l1_background.jpg'); //unique to each round
        game.load.image('question1', 'assets/1_1.png'); //unique to each round
        game.load.image('w1_1o1', 'assets/w1_1o1.png'); //unique to each round
        game.load.image('r1_1o2', 'assets/r1_1o2.png'); //unique to each round
        game.load.image('w1_1o3', 'assets/w1_1o3.png'); //unique to each round
        game.load.image('w1_1o4', 'assets/w1_1o4.png'); //unique to each round
        game.load.image('r1_1o2r', 'assets/r1_1o2r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_1_1 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_1_1');
        this.player1_1_1.scale.x = 0.083;
        this.player1_1_1.scale.y = 0.162;

        this.player2_1_1 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_1_1');
        this.player2_1_1.scale.x = 0.083;
        this.player2_1_1.scale.y = 0.162;

        this.q1 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q1');
        this.q1.scale.x = 0.538;
        this.q1.scale.y = 0.55;

        this.bar1_1_1 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_1_1');
        this.bar1_1_1.scale.x = 0.3;
        this.bar1_1_1.scale.y = 0.415;

        this.bar2_1_1 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_1_1');
        this.bar2_1_1.scale.x = 0.3;
        this.bar2_1_1.scale.y = 0.415;

        this.timeInSeconds_OP_1_1 = 120;
        this.timer_OP_1_1 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_1_1--;
        var minutes_OP_1_1 = Math.floor(this.timeInSeconds_OP_1_1 / 10);
        var seconds_OP_1_1 = this.timeInSeconds_OP_1_1 - (minutes_OP_1_1 * 10);
    
        if(seconds_OP_1_1 == 8) {
            this.show_OP();
            this.q1.kill();
            game.time.events.remove(this.timer_OP_1_1);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_1_1--;
        var minutes_1_1 = Math.floor(this.timeInSeconds_1_1 / 10);
        var seconds_1_1 = this.timeInSeconds_1_1 - (minutes_1_1 * 10);
        var timeString_1_1 = this.addZeros(seconds_1_1);
        this.timeText_1_1.text = timeString_1_1;
    
        if(seconds_1_1 == 9 && !destroyed) {
            this.wrong_bar_1_p2_1_1 = new Phaser.Rectangle(285, 288, 10, (1000 - player2_score) / 1000 * 202);
            this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_1_1, this);
            player2_answered = true;
        }

        if (seconds_1_1 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_1_1 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_1o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_1, this);
            }
            else {
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_1o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_1r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_1_1r() {
        game.time.events.remove(this.timer_1_1);
        this.timeText_1_1.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l1_2');
    },
    destroy_rectangle_p1_nextLevel_1_1() {
        this.wrong_bar_1_p1_1_1.width = 0;
        this.wrong_bar_1_p1_1_1.length = 0;
        game.time.events.remove(this.timer_1_1);
        this.timeText_1_1.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l1_2');
    },
    destroy_rectangle_p1_1_1() {
        this.wrong_bar_1_p1_1_1.width = 0;
        this.wrong_bar_1_p1_1_1.length = 0;
    },
    destroy_rectangle_p2_1_1() {
        this.wrong_bar_1_p2_1_1.width = 0;
        this.wrong_bar_1_p2_1_1.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_1_1 = 120;
        this.timeText_1_1 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_1_1.anchor.set(0.5, 0.5);
        this.timer_1_1 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question1 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question1');
        this.question1.scale.x = 0.2;
        this.question1.scale.y = 0.215;

        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q1_1o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_1_1, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q1_1o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_1_1, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q1_1o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_1_1, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q1_1o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_1_1, this);
    },
    clickOption1_1_1: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+7, 'w1_1o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_1 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_1, this);
        player1_answered = true;
    },
    clickOption2_1_1: function() {
        this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_1o2');
        this.r2.scale.x = 0.477;
        this.r2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.correct_bar_1_p1_1_1 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption3_1_1: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+119, 'w1_1o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_1 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_1, this);
        player1_answered = true;
    },
    clickOption4_1_1: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'w1_1o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_1 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_1, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_1_1, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_1_1, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_1_1, '#33FFC6');
    }
};
var l1_2 = {
    preload: function() {
        game.load.image('player1_1_2', 'assets/l1_p1.jpg');
        game.load.image('player2_1_2', 'assets/l1_p2.jpg');
        game.load.image('bar_1_2', 'assets/bar.png');
        game.load.image('option_1_2', 'assets/option.png');
        game.load.image('q1_2o1', 'assets/q1_2o1.png'); //unique to each round
        game.load.image('q1_2o2', 'assets/q1_2o2.png'); //unique to each round
        game.load.image('q1_2o3', 'assets/q1_2o3.png'); //unique to each round
        game.load.image('q1_2o4', 'assets/q1_2o4.png'); //unique to each round
        game.load.image('gaming_1_2', 'assets/game_situation.jpg');
        game.load.image('q2', 'assets/question_2.png'); //unique to each round
        game.load.image('firstq', 'assets/l1_background.jpg'); //background
        game.load.image('question2', 'assets/1_2.png'); //unique to each round
        game.load.image('r1_2o1', 'assets/r1_2o1.png'); //unique to each round
        game.load.image('w1_2o2', 'assets/w1_2o2.png'); //unique to each round
        game.load.image('w1_2o3', 'assets/w1_2o3.png'); //unique to each round
        game.load.image('w1_2o4', 'assets/w1_2o4.png'); //unique to each round
        game.load.image('r1_2o1r', 'assets/r1_2o1r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_1_2 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_1_2');
        this.player1_1_2.scale.x = 0.083;
        this.player1_1_2.scale.y = 0.162;

        this.player2_1_2 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_1_2');
        this.player2_1_2.scale.x = 0.083;
        this.player2_1_2.scale.y = 0.162;

        this.correct_bar_1_p1_1_2 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1000 * 202));
        this.correct_bar_1_p2_1_2 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));

        this.q2 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q2');
        this.q2.scale.x = 0.538;
        this.q2.scale.y = 0.55;

        this.bar1_1_2 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_1_2');
        this.bar1_1_2.scale.x = 0.3;
        this.bar1_1_2.scale.y = 0.415;

        this.bar2_1_2 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_1_2');
        this.bar2_1_2.scale.x = 0.3;
        this.bar2_1_2.scale.y = 0.415;

        this.timeInSeconds_OP_1_2 = 120;
        this.timer_OP_1_2 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_1_2--;
        var minutes_OP_1_2 = Math.floor(this.timeInSeconds_OP_1_2 / 10);
        var seconds_OP_1_2 = this.timeInSeconds_OP_1_2 - (minutes_OP_1_2 * 10);
    
        if(seconds_OP_1_2 == 8) {
            this.show_OP();
            this.q2.kill();
            game.time.events.remove(this.timer_OP_1_2);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_1_2--;
        var minutes_1_2 = Math.floor(this.timeInSeconds_1_2 / 10);
        var seconds_1_2 = this.timeInSeconds_1_2 - (minutes_1_2 * 10);
        var timeString_1_2 = this.addZeros(seconds_1_2);
        this.timeText_1_2.text = timeString_1_2;
    
        if(seconds_1_2 == 7 && !destroyed) {
            this.wrong_bar_1_p2_1_2 = new Phaser.Rectangle(285, 288, 10, (1000 - player2_score) / 1000 * 202);
            this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_1_2, this);
            player2_answered = true;
        }

        if (seconds_1_2 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_1_2 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+7, 'r1_2o1r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_2, this);
            }
            else {
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+7, 'r1_2o1r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_2r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_1_2r() {
        game.time.events.remove(this.timer_1_2);
        this.timeText_1_2.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l1_3');
    },
    destroy_rectangle_p1_nextLevel_1_2() {
        this.wrong_bar_1_p1_1_2.width = 0;
        this.wrong_bar_1_p1_1_2.length = 0;
        game.time.events.remove(this.timer_1_2);
        this.timeText_1_2.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l1_3');
    },
    destroy_rectangle_p1_1_2() {
        this.wrong_bar_1_p1_1_2.width = 0;
        this.wrong_bar_1_p1_1_2.length = 0;
    },
    destroy_rectangle_p2_1_2() {
        this.wrong_bar_1_p2_1_2.width = 0;
        this.wrong_bar_1_p2_1_2.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_1_2 = 120;
        this.timeText_1_2 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_1_2.anchor.set(0.5, 0.5);
        this.timer_1_2 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question2 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question2'); //要改成第n題
        this.question2.scale.x = 0.2;
        this.question2.scale.y = 0.215;

        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q1_2o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_1_2, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q1_2o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_1_2, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q1_2o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_1_2, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q1_2o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_1_2, this);
    },
    clickOption1_1_2: function() {
        this.r2 = game.add.sprite(game.width/2-102, game.height/2+7, 'r1_2o1');
        this.r2.scale.x = 0.477;
        this.r2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_1_2 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption2_1_2: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+63, 'w1_2o2');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_2 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_2, this);
        player1_answered = true;
    },
    clickOption3_1_2: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+119, 'w1_2o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_2 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_2, this);
        player1_answered = true;
    },
    clickOption4_1_2: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'w1_2o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_2 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_2, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_1_2, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_1_2, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_1_2, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_1_2, '#33FFC6');
    }
};
var l1_3 = {
    preload: function() {
        game.load.image('player1_1_3', 'assets/l1_p1.jpg');
        game.load.image('player2_1_3', 'assets/l1_p2.jpg');
        game.load.image('bar_1_3', 'assets/bar.png');
        game.load.image('option_1_3', 'assets/option.png');
        game.load.image('q1_3o1', 'assets/q1_3o1.png'); //unique to each round
        game.load.image('q1_3o2', 'assets/q1_3o2.png'); //unique to each round
        game.load.image('q1_3o3', 'assets/q1_3o3.png'); //unique to each round
        game.load.image('q1_3o4', 'assets/q1_3o4.png'); //unique to each round
        game.load.image('gaming_1_3', 'assets/game_situation.jpg');
        game.load.image('q3', 'assets/question_3.png'); //unique to each round
        game.load.image('firstq', 'assets/l1_background.jpg'); //background
        game.load.image('question3', 'assets/1_3.png'); //unique to each round
        game.load.image('w1_3o1', 'assets/w1_3o1.png'); //unique to each round
        game.load.image('r1_3o2', 'assets/r1_3o2.png'); //unique to each round
        game.load.image('w1_3o3', 'assets/w1_3o3.png'); //unique to each round
        game.load.image('w1_3o4', 'assets/w1_3o4.png'); //unique to each round
        game.load.image('r1_3o2r', 'assets/r1_3o2r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_1_3 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_1_3');
        this.player1_1_3.scale.x = 0.083;
        this.player1_1_3.scale.y = 0.162;

        this.player2_1_3 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_1_3');
        this.player2_1_3.scale.x = 0.083;
        this.player2_1_3.scale.y = 0.162;

        this.correct_bar_1_p1_1_3 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1000 * 202));
        this.correct_bar_1_p2_1_3 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));

        this.q3 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q3'); //don't forget to modify!!!
        this.q3.scale.x = 0.538;
        this.q3.scale.y = 0.55;

        this.bar1_1_3 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_1_3');
        this.bar1_1_3.scale.x = 0.3;
        this.bar1_1_3.scale.y = 0.415;

        this.bar2_1_3 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_1_3');
        this.bar2_1_3.scale.x = 0.3;
        this.bar2_1_3.scale.y = 0.415;

        this.timeInSeconds_OP_1_3 = 120;
        this.timer_OP_1_3 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_1_3--;
        var minutes_OP_1_3 = Math.floor(this.timeInSeconds_OP_1_3 / 10);
        var seconds_OP_1_3 = this.timeInSeconds_OP_1_3 - (minutes_OP_1_3 * 10);
    
        if(seconds_OP_1_3 == 8) {
            this.show_OP();
            this.q3.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_1_3);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_1_3--;
        var minutes_1_3 = Math.floor(this.timeInSeconds_1_3 / 10);
        var seconds_1_3 = this.timeInSeconds_1_3 - (minutes_1_3 * 10);
        var timeString_1_3 = this.addZeros(seconds_1_3);
        this.timeText_1_3.text = timeString_1_3;
    
        if(seconds_1_3 == 4 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_1_3 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_1_3, this);
            player2_answered = true;
        }

        if (seconds_1_3 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_1_3 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_3o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_3, this);
            }
            else {
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_3o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_3r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_1_3r() {
        game.time.events.remove(this.timer_1_3);
        this.timeText_1_3.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l1_4');
    },
    destroy_rectangle_p1_nextLevel_1_3() {
        this.wrong_bar_1_p1_1_3.width = 0;
        this.wrong_bar_1_p1_1_3.length = 0;
        game.time.events.remove(this.timer_1_3);
        this.timeText_1_3.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l1_4');
    },
    destroy_rectangle_p1_1_3() {
        this.wrong_bar_1_p1_1_3.width = 0;
        this.wrong_bar_1_p1_1_3.length = 0;
    },
    destroy_rectangle_p2_1_3() {
        this.wrong_bar_1_p2_1_3.width = 0;
        this.wrong_bar_1_p2_1_3.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_1_3 = 120;
        this.timeText_1_3 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_1_3.anchor.set(0.5, 0.5);
        this.timer_1_3 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question3 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question3'); //要改成第n題
        this.question3.scale.x = 0.2;
        this.question3.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q1_3o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_1_3, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q1_3o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_1_3, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q1_3o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_1_3, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q1_3o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_1_3, this);
    },
    clickOption1_1_3: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+7, 'w1_3o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_3 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_3, this);
        player1_answered = true;
    },
    clickOption2_1_3: function() {
        this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_3o2');
        this.r2.scale.x = 0.477;
        this.r2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_1_3 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption3_1_3: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+119, 'w1_3o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_3 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_3, this);
        player1_answered = true;
    },
    clickOption4_1_3: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'w1_3o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_3 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_3, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_1_3, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_1_3, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_1_3, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_1_3, '#33FFC6');
    }
};
var l1_4 = {
    preload: function() {
        game.load.image('player1_1_4', 'assets/l1_p1.jpg');
        game.load.image('player2_1_4', 'assets/l1_p2.jpg');
        game.load.image('bar_1_4', 'assets/bar.png');
        game.load.image('option_1_4', 'assets/option.png');
        game.load.image('q1_4o1', 'assets/q1_4o1.png'); //unique to each round
        game.load.image('q1_4o2', 'assets/q1_4o2.png'); //unique to each round
        game.load.image('q1_4o3', 'assets/q1_4o3.png'); //unique to each round
        game.load.image('q1_4o4', 'assets/q1_4o4.png'); //unique to each round
        game.load.image('gaming_1_4', 'assets/game_situation.jpg');
        game.load.image('q4', 'assets/question_4.png'); //unique to each round
        game.load.image('firstq', 'assets/l1_background.jpg'); //background
        game.load.image('question4', 'assets/1_4.png'); //unique to each round
        game.load.image('w1_4o1', 'assets/w1_4o1.png'); //unique to each round
        game.load.image('r1_4o2', 'assets/r1_4o2.png'); //unique to each round
        game.load.image('w1_4o3', 'assets/w1_4o3.png'); //unique to each round
        game.load.image('w1_4o4', 'assets/w1_4o4.png'); //unique to each round
        game.load.image('r1_4o2r', 'assets/r1_4o2r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_1_4 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_1_4');
        this.player1_1_4.scale.x = 0.083;
        this.player1_1_4.scale.y = 0.162;

        this.player2_1_4 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_1_4');
        this.player2_1_4.scale.x = 0.083;
        this.player2_1_4.scale.y = 0.162;

        this.correct_bar_1_p1_1_4 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1000 * 202));
        this.correct_bar_1_p2_1_4 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));

        this.q4 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q4'); //don't forget to modify!!!
        this.q4.scale.x = 0.538;
        this.q4.scale.y = 0.55;

        this.bar1_1_4 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_1_4');
        this.bar1_1_4.scale.x = 0.3;
        this.bar1_1_4.scale.y = 0.415;

        this.bar2_1_4 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_1_4');
        this.bar2_1_4.scale.x = 0.3;
        this.bar2_1_4.scale.y = 0.415;

        this.timeInSeconds_OP_1_4 = 120;
        this.timer_OP_1_4 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_1_4--;
        var minutes_OP_1_4 = Math.floor(this.timeInSeconds_OP_1_4 / 10);
        var seconds_OP_1_4 = this.timeInSeconds_OP_1_4 - (minutes_OP_1_4 * 10);
    
        if(seconds_OP_1_4 == 8) {
            this.show_OP();
            this.q4.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_1_4);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_1_4--;
        var minutes_1_4 = Math.floor(this.timeInSeconds_1_4 / 10);
        var seconds_1_4 = this.timeInSeconds_1_4 - (minutes_1_4 * 10);
        var timeString_1_4 = this.addZeros(seconds_1_4);
        this.timeText_1_4.text = timeString_1_4;
    
        if(seconds_1_4 == 7 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_1_4 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_1_4, this);
            player2_answered = true;
        }

        if (seconds_1_4 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_1_4 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r1 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_4o2r');
                this.r1.scale.x = 0.477;
                this.r1.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_4, this);
            }
            else {
                this.r1 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_4o2r');
                this.r1.scale.x = 0.477;
                this.r1.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_4r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_1_4r() {
        game.time.events.remove(this.timer_1_4);
        this.timeText_1_4.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l1_5');
    },
    destroy_rectangle_p1_nextLevel_1_4() {
        this.wrong_bar_1_p1_1_4.width = 0;
        this.wrong_bar_1_p1_1_4.length = 0;
        game.time.events.remove(this.timer_1_4);
        this.timeText_1_4.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l1_5');
    },
    destroy_rectangle_p1_1_4() {
        this.wrong_bar_1_p1_1_4.width = 0;
        this.wrong_bar_1_p1_1_4.length = 0;
    },
    destroy_rectangle_p2_1_4() {
        this.wrong_bar_1_p2_1_4.width = 0;
        this.wrong_bar_1_p2_1_4.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_1_4 = 120;
        this.timeText_1_4 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_1_4.anchor.set(0.5, 0.5);
        this.timer_1_4 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question4 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question4'); //要改成第n題
        this.question4.scale.x = 0.2;
        this.question4.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q1_4o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_1_4, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q1_4o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_1_4, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q1_4o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_1_4, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q1_4o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_1_4, this);
    },
    clickOption1_1_4: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+7, 'w1_4o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_4 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_4, this);
        player1_answered = true;
    },
    clickOption2_1_4: function() {
        this.r1 = game.add.sprite(game.width/2-102, game.height/2+63, 'r1_4o2');
        this.r1.scale.x = 0.477;
        this.r1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_1_4 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption3_1_4: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+119, 'w1_4o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_4 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_4, this);
        player1_answered = true;
    },
    clickOption4_1_4: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'w1_4o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_4 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_4, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_1_4, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_1_4, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_1_4, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_1_4, '#33FFC6');
    }
};
var l1_5 = {
    preload: function() {
        game.load.image('player1_1_5', 'assets/l1_p1.jpg');
        game.load.image('player2_1_5', 'assets/l1_p2.jpg');
        game.load.image('bar_1_5', 'assets/bar.png');
        game.load.image('option_1_5', 'assets/option.png');
        game.load.image('q1_5o1', 'assets/q1_5o1.png'); //unique to each round
        game.load.image('q1_5o2', 'assets/q1_5o2.png'); //unique to each round
        game.load.image('q1_5o3', 'assets/q1_5o3.png'); //unique to each round
        game.load.image('q1_5o4', 'assets/q1_5o4.png'); //unique to each round
        game.load.image('gaming_1_5', 'assets/game_situation.jpg');
        game.load.image('q5', 'assets/last_question.png'); 
        game.load.image('firstq', 'assets/l1_background.jpg'); //background
        game.load.image('question5', 'assets/1_5.png'); 
        game.load.image('w1_5o1', 'assets/w1_5o1.png'); //unique to each round
        game.load.image('w1_5o2', 'assets/w1_5o2.png'); //unique to each round
        game.load.image('r1_5o3', 'assets/r1_5o3.png'); //unique to each round
        game.load.image('w1_5o4', 'assets/w1_5o4.png'); //unique to each round
        game.load.image('r1_5o3r', 'assets/r1_5o3r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_1_5 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_1_5');
        this.player1_1_5.scale.x = 0.083;
        this.player1_1_5.scale.y = 0.162;

        this.player2_1_5 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_1_5');
        this.player2_1_5.scale.x = 0.083;
        this.player2_1_5.scale.y = 0.162;

        this.correct_bar_1_p1_1_5 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1000 * 202));
        this.correct_bar_1_p2_1_5 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));

        this.q5 = game.add.sprite(game.width/2 - 125, game.height/2 - 127, 'q5'); //don't forget to modify!!!
        this.q5.scale.x = 0.538;
        this.q5.scale.y = 0.55;

        this.bar1_1_5 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_1_5');
        this.bar1_1_5.scale.x = 0.3;
        this.bar1_1_5.scale.y = 0.415;

        this.bar2_1_5 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_1_5');
        this.bar2_1_5.scale.x = 0.3;
        this.bar2_1_5.scale.y = 0.415;

        this.timeInSeconds_OP_1_5 = 120;
        this.timer_OP_1_5 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_1_5--;
        var minutes_OP_1_5 = Math.floor(this.timeInSeconds_OP_1_5 / 10);
        var seconds_OP_1_5 = this.timeInSeconds_OP_1_5 - (minutes_OP_1_5 * 10);
    
        if(seconds_OP_1_5 == 8) {
            this.show_OP();
            this.q5.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_1_5);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_1_5--;
        var minutes_1_5 = Math.floor(this.timeInSeconds_1_5 / 10);
        var seconds_1_5 = this.timeInSeconds_1_5 - (minutes_1_5 * 10);
        var timeString_1_5 = this.addZeros(seconds_1_5);
        this.timeText_1_5.text = timeString_1_5;
    
        if(seconds_1_5 == 5 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_1_5 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_1_4, this);
            player2_answered = true;
        }

        if (seconds_1_5 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_1_5 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r3 = game.add.sprite(game.width/2-102, game.height/2+119, 'r1_5o3r');
                this.r3.scale.x = 0.477;
                this.r3.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_5, this);
            }
            else {
                this.r3 = game.add.sprite(game.width/2-102, game.height/2+119, 'r1_5o3r');
                this.r3.scale.x = 0.477;
                this.r3.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_1_5r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_1_5r() {
        game.time.events.remove(this.timer_1_5);
        this.timeText_1_5.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('round_over');
    },
    destroy_rectangle_p1_nextLevel_1_5() {
        this.wrong_bar_1_p1_1_5.width = 0;
        this.wrong_bar_1_p1_1_5.length = 0;
        game.time.events.remove(this.timer_1_5);
        this.timeText_1_5.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('round_over');
    },
    destroy_rectangle_p1_1_5() {
        this.wrong_bar_1_p1_1_5.width = 0;
        this.wrong_bar_1_p1_1_5.length = 0;
    },
    destroy_rectangle_p2_1_5() {
        this.wrong_bar_1_p2_1_5.width = 0;
        this.wrong_bar_1_p2_1_5.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_1_5 = 120;
        this.timeText_1_5 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_1_5.anchor.set(0.5, 0.5);
        this.timer_1_5 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question5 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question5'); //要改成第n題
        this.question5.scale.x = 0.2;
        this.question5.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q1_5o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_1_5, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q1_5o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_1_5, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q1_5o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_1_5, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q1_5o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_1_5, this);
    },
    clickOption1_1_5: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+7, 'w1_5o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_5 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_5, this);
        player1_answered = true;
    },
    clickOption2_1_5: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+63, 'w1_5o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_5 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_5, this);
        player1_answered = true;
    },
    clickOption3_1_5: function() {
        this.r3 = game.add.sprite(game.width/2-102, game.height/2+119, 'r1_5o3');
        this.r3.scale.x = 0.477;
        this.r3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_1_5 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption4_1_5: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'w1_5o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_1_5 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_1_5, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_1_5, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_1_5, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_1_5, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_1_5, '#33FFC6');
    }
}
var l2_1 = {
    preload: function() {
        game.load.image('player1_2_1', 'assets/l2_p1.jpg');
        game.load.image('player2_2_1', 'assets/l2_p2.jpg');
        game.load.image('bar_2_1', 'assets/bar.png');
        game.load.image('option_2_1', 'assets/option.png');
        game.load.image('q2_1o1', 'assets/q2_1o1.png'); //unique to each round
        game.load.image('q2_1o2', 'assets/q2_1o2.png'); //unique to each round
        game.load.image('q2_1o3', 'assets/q2_1o3.png'); //unique to each round
        game.load.image('q2_1o4', 'assets/q2_1o4.png'); //unique to each round
        game.load.image('gaming_2_1', 'assets/game_situation.jpg');
        game.load.image('q1', 'assets/question_1.png'); //unique to each round
        game.load.image('firstq', 'assets/l2_background.jpg'); //unique to each round
        game.load.image('question1', 'assets/2_1.png'); //unique to each round
        game.load.image('w2_1o1', 'assets/w2_1o1.png'); //unique to each round
        game.load.image('r2_1o2', 'assets/r2_1o2.png'); //unique to each round
        game.load.image('w2_1o3', 'assets/w2_1o3.png'); //unique to each round
        game.load.image('w2_1o4', 'assets/w2_1o4.png'); //unique to each round
        game.load.image('r2_1o2r', 'assets/r2_1o2r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_2_1 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_2_1');
        this.player1_2_1.scale.x = 0.083;
        this.player1_2_1.scale.y = 0.162;

        this.player2_2_1 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_2_1');
        this.player2_2_1.scale.x = 0.083;
        this.player2_2_1.scale.y = 0.162;

        this.q1 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q1');
        this.q1.scale.x = 0.538;
        this.q1.scale.y = 0.55;

        this.bar1_2_1 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_2_1');
        this.bar1_2_1.scale.x = 0.3;
        this.bar1_2_1.scale.y = 0.415;

        this.bar2_2_1 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_2_1');
        this.bar2_2_1.scale.x = 0.3;
        this.bar2_2_1.scale.y = 0.415;

        this.timeInSeconds_OP_2_1 = 120;
        this.timer_OP_2_1 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_2_1--;
        var minutes_OP_2_1 = Math.floor(this.timeInSeconds_OP_2_1 / 10);
        var seconds_OP_2_1 = this.timeInSeconds_OP_2_1 - (minutes_OP_2_1 * 10);
    
        if(seconds_OP_2_1 == 8) {
            this.show_OP();
            this.q1.kill();
            game.time.events.remove(this.timer_OP_2_1);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_2_1--;
        var minutes_2_1 = Math.floor(this.timeInSeconds_2_1 / 10);
        var seconds_2_1 = this.timeInSeconds_2_1 - (minutes_2_1 * 10);
        var timeString_2_1 = this.addZeros(seconds_2_1);
        this.timeText_2_1.text = timeString_2_1;
    
        if(seconds_2_1 == 7 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_2_1 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_2_1, this);
            player2_answered = true;
        }

        if (seconds_2_1 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_2_1 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r2_1o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_1, this);
            }
            else {
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r2_1o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_1r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_2_1r() {
        game.time.events.remove(this.timer_2_1);
        this.timeText_2_1.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l2_2');
    },
    destroy_rectangle_p1_nextLevel_2_1() {
        this.wrong_bar_1_p1_2_1.width = 0;
        this.wrong_bar_1_p1_2_1.length = 0;
        game.time.events.remove(this.timer_2_1);
        this.timeText_2_1.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l2_2');
    },
    destroy_rectangle_p1_2_1() {
        this.wrong_bar_1_p1_2_1.width = 0;
        this.wrong_bar_1_p1_2_1.length = 0;
    },
    destroy_rectangle_p2_2_1() {
        this.wrong_bar_1_p2_2_1.width = 0;
        this.wrong_bar_1_p2_2_1.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_2_1 = 120;
        this.timeText_2_1 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_2_1.anchor.set(0.5, 0.5);
        this.timer_2_1 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question1 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question1');
        this.question1.scale.x = 0.2;
        this.question1.scale.y = 0.215;

        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q2_1o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_2_1, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q2_1o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_2_1, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q2_1o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_2_1, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q2_1o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_2_1, this);
    },
    clickOption1_2_1: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+7, 'w2_1o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_1 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_1, this);
        player1_answered = true;
    },
    clickOption2_2_1: function() {
        this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r2_1o2');
        this.r2.scale.x = 0.477;
        this.r2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.correct_bar_1_p1_2_1 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption3_2_1: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+119, 'w2_1o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_1 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_1, this);
        player1_answered = true;
    },
    clickOption4_2_1: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'w2_1o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_1 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_1, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_2_1, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_2_1, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_2_1, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_2_1, '#33FFC6');
    }
};
var l2_2 = {
    preload: function() {
        game.load.image('player1_2_2', 'assets/l2_p1.jpg');
        game.load.image('player2_2_2', 'assets/l2_p2.jpg');
        game.load.image('bar_2_2', 'assets/bar.png');
        game.load.image('option_2_2', 'assets/option.png');
        game.load.image('q2_2o1', 'assets/q2_2o1.png'); //unique to each round
        game.load.image('q2_2o2', 'assets/q2_2o2.png'); //unique to each round
        game.load.image('q2_2o3', 'assets/q2_2o3.png'); //unique to each round
        game.load.image('q2_2o4', 'assets/q2_2o4.png'); //unique to each round
        game.load.image('gaming_2_2', 'assets/game_situation.jpg');
        game.load.image('q2', 'assets/question_2.png'); //unique to each round
        game.load.image('firstq', 'assets/l2_background.jpg'); //background
        game.load.image('question2', 'assets/2_2.png'); //unique to each round
        game.load.image('w2_2o1', 'assets/w2_2o1.png'); //unique to each round
        game.load.image('w2_2o2', 'assets/w2_2o2.png'); //unique to each round
        game.load.image('w2_2o3', 'assets/w2_2o3.png'); //unique to each round
        game.load.image('r2_2o4', 'assets/r2_2o4.png'); //unique to each round
        game.load.image('r2_2o4r', 'assets/r2_2o4r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_2_2 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_2_2');
        this.player1_2_2.scale.x = 0.083;
        this.player1_2_2.scale.y = 0.162;

        this.player2_2_2 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_2_2');
        this.player2_2_2.scale.x = 0.083;
        this.player2_2_2.scale.y = 0.162;

        this.correct_bar_1_p1_2_2 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1000 * 202));
        this.correct_bar_1_p2_2_2 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));

        this.q2 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q2');
        this.q2.scale.x = 0.538;
        this.q2.scale.y = 0.55;

        this.bar1_2_2 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_2_2');
        this.bar1_2_2.scale.x = 0.3;
        this.bar1_2_2.scale.y = 0.415;

        this.bar2_2_2 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_2_2');
        this.bar2_2_2.scale.x = 0.3;
        this.bar2_2_2.scale.y = 0.415;

        this.timeInSeconds_OP_2_2 = 120;
        this.timer_OP_2_2 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_2_2--;
        var minutes_OP_2_2 = Math.floor(this.timeInSeconds_OP_2_2 / 10);
        var seconds_OP_2_2 = this.timeInSeconds_OP_2_2 - (minutes_OP_2_2 * 10);
    
        if(seconds_OP_2_2 == 8) {
            this.show_OP();
            this.q2.kill();
            game.time.events.remove(this.timer_OP_2_2);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_2_2--;
        var minutes_2_2 = Math.floor(this.timeInSeconds_2_2 / 10);
        var seconds_2_2 = this.timeInSeconds_2_2 - (minutes_2_2 * 10);
        var timeString_2_2 = this.addZeros(seconds_2_2);
        this.timeText_2_2.text = timeString_2_2;
    
        if(seconds_2_2 == 9 && !destroyed) {
            this.wrong_bar_1_p2_2_2 = new Phaser.Rectangle(285, 288, 10, (1000 - player2_score) / 1000 * 202);
            this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_2_2, this);
            player2_answered = true;
        }

        if (seconds_2_2 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_2_2 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'r2_2o4r');
                this.r4.scale.x = 0.477;
                this.r4.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_2, this);
            }
            else {
                this.r4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'r2_2o4r');
                this.r4.scale.x = 0.477;
                this.r4.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_2r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_2_2r() {
        game.time.events.remove(this.timer_2_2);
        this.timeText_2_2.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l2_3');
    },
    destroy_rectangle_p1_nextLevel_2_2() {
        this.wrong_bar_1_p1_2_2.width = 0;
        this.wrong_bar_1_p1_2_2.length = 0;
        game.time.events.remove(this.timer_2_2);
        this.timeText_2_2.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l2_3');
    },
    destroy_rectangle_p1_2_2() {
        this.wrong_bar_1_p1_2_2.width = 0;
        this.wrong_bar_1_p1_2_2.length = 0;
    },
    destroy_rectangle_p2_2_2() {
        this.wrong_bar_1_p2_2_2.width = 0;
        this.wrong_bar_1_p2_2_2.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_2_2 = 120;
        this.timeText_2_2 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_2_2.anchor.set(0.5, 0.5);
        this.timer_2_2 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question2 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question2'); //要改成第n題
        this.question2.scale.x = 0.2;
        this.question2.scale.y = 0.215;

        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q2_2o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_2_2, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q2_2o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_2_2, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q2_2o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_2_2, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q2_2o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_2_2, this);
    },
    clickOption1_2_2: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+7, 'w2_2o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_2 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_2, this);
        player1_answered = true;
    },
    clickOption2_2_2: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+63, 'w2_2o2');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_2 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_2, this);
        player1_answered = true;
    },
    clickOption3_2_2: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+119, 'w2_2o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_2 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_2, this);
        player1_answered = true;
    },
    clickOption4_2_2: function() {
        this.r4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'r2_2o4');
        this.r4.scale.x = 0.477;
        this.r4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_2_2 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_2_2, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_2_2, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_2_2, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_2_2, '#33FFC6');
    }
};
var l2_3 = {
    preload: function() {
        game.load.image('player1_2_3', 'assets/l2_p1.jpg');
        game.load.image('player2_2_3', 'assets/l2_p2.jpg');
        game.load.image('bar_2_3', 'assets/bar.png');
        game.load.image('option_2_3', 'assets/option.png');
        game.load.image('q2_3o1', 'assets/q2_3o1.png'); //unique to each round
        game.load.image('q2_3o2', 'assets/q2_3o2.png'); //unique to each round
        game.load.image('q2_3o3', 'assets/q2_3o3.png'); //unique to each round
        game.load.image('q2_3o4', 'assets/q2_3o4.png'); //unique to each round
        game.load.image('gaming_2_3', 'assets/game_situation.jpg');
        game.load.image('q3', 'assets/question_3.png'); //unique to each round
        game.load.image('firstq', 'assets/l2_background.jpg'); //background
        game.load.image('question3', 'assets/2_3.png'); //unique to each round
        game.load.image('w2_3o1', 'assets/w2_3o1.png'); //unique to each round
        game.load.image('w2_3o2', 'assets/w2_3o2.png'); //unique to each round
        game.load.image('r2_3o3', 'assets/r2_3o3.png'); //unique to each round
        game.load.image('w2_3o4', 'assets/w2_3o4.png'); //unique to each round
        game.load.image('r2_3o3r', 'assets/r2_3o3r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_2_3 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_2_3');
        this.player1_2_3.scale.x = 0.083;
        this.player1_2_3.scale.y = 0.162;

        this.player2_2_3 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_2_3');
        this.player2_2_3.scale.x = 0.083;
        this.player2_2_3.scale.y = 0.162;

        this.correct_bar_1_p1_2_3 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1000 * 202));
        this.correct_bar_1_p2_2_3 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));

        this.q3 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q3'); //don't forget to modify!!!
        this.q3.scale.x = 0.538;
        this.q3.scale.y = 0.55;

        this.bar1_2_3 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_2_3');
        this.bar1_2_3.scale.x = 0.3;
        this.bar1_2_3.scale.y = 0.415;

        this.bar2_2_3 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_2_3');
        this.bar2_2_3.scale.x = 0.3;
        this.bar2_2_3.scale.y = 0.415;

        this.timeInSeconds_OP_2_3 = 120;
        this.timer_OP_2_3 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_2_3--;
        var minutes_OP_2_3 = Math.floor(this.timeInSeconds_OP_2_3 / 10);
        var seconds_OP_2_3 = this.timeInSeconds_OP_2_3 - (minutes_OP_2_3 * 10);
    
        if(seconds_OP_2_3 == 8) {
            this.show_OP();
            this.q3.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_2_3);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_2_3--;
        var minutes_2_3 = Math.floor(this.timeInSeconds_2_3 / 10);
        var seconds_2_3 = this.timeInSeconds_2_3 - (minutes_2_3 * 10);
        var timeString_2_3 = this.addZeros(seconds_2_3);
        this.timeText_2_3.text = timeString_2_3;
    
        if(seconds_2_3 == 4 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_2_3 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_2_3, this);
            player2_answered = true;
        }

        if (seconds_2_3 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_2_3 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r3 = game.add.sprite(game.width/2-102, game.height/2+119, 'r2_3o3r');
                this.r3.scale.x = 0.477;
                this.r3.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_3, this);
            }
            else {
                this.r3 = game.add.sprite(game.width/2-102, game.height/2+119, 'r2_3o3r');
                this.r3.scale.x = 0.477;
                this.r3.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_3r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_2_3r() {
        game.time.events.remove(this.timer_2_3);
        this.timeText_2_3.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l2_4');
    },
    destroy_rectangle_p1_nextLevel_2_3() {
        this.wrong_bar_1_p1_2_3.width = 0;
        this.wrong_bar_1_p1_2_3.length = 0;
        game.time.events.remove(this.timer_2_3);
        this.timeText_2_3.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l2_4');
    },
    destroy_rectangle_p1_2_3() {
        this.wrong_bar_1_p1_2_3.width = 0;
        this.wrong_bar_1_p1_2_3.length = 0;
    },
    destroy_rectangle_p2_2_3() {
        this.wrong_bar_1_p2_2_3.width = 0;
        this.wrong_bar_1_p2_2_3.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_2_3 = 120;
        this.timeText_2_3 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_2_3.anchor.set(0.5, 0.5);
        this.timer_2_3 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question3 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question3'); //要改成第n題
        this.question3.scale.x = 0.2;
        this.question3.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q2_3o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_2_3, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q2_3o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_2_3, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q2_3o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_2_3, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q2_3o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_2_3, this);
    },
    clickOption1_2_3: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+7, 'w2_3o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_3 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_3, this);
        player1_answered = true;
    },
    clickOption2_2_3: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+63, 'w2_3o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_3 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_3, this);
        player1_answered = true;
    },
    clickOption3_2_3: function() {
        this.r3 = game.add.sprite(game.width/2-102, game.height/2+119, 'r2_3o3');
        this.r3.scale.x = 0.477;
        this.r3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.correct_bar_1_p1_2_3 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption4_2_3: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'w2_3o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_3 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_3, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_2_3, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_2_3, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_2_3, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_2_3, '#33FFC6');
    }
};
var l2_4 = {
    preload: function() {
        game.load.image('player1_2_4', 'assets/l2_p1.jpg');
        game.load.image('player2_2_4', 'assets/l2_p2.jpg');
        game.load.image('bar_2_4', 'assets/bar.png');
        game.load.image('option_2_4', 'assets/option.png');
        game.load.image('q2_4o1', 'assets/q2_4o1.png'); //unique to each round
        game.load.image('q2_4o2', 'assets/q2_4o2.png'); //unique to each round
        game.load.image('q2_4o3', 'assets/q2_4o3.png'); //unique to each round
        game.load.image('q2_4o4', 'assets/q2_4o4.png'); //unique to each round
        game.load.image('gaming_2_4', 'assets/game_situation.jpg');
        game.load.image('q4', 'assets/question_4.png'); //unique to each round
        game.load.image('firstq', 'assets/l2_background.jpg'); //background
        game.load.image('question4', 'assets/2_4.png'); //unique to each round
        game.load.image('w2_4o1', 'assets/w2_4o1.png'); //unique to each round
        game.load.image('w2_4o2', 'assets/w2_4o2.png'); //unique to each round
        game.load.image('w2_4o3', 'assets/w2_4o3.png'); //unique to each round
        game.load.image('r2_4o4', 'assets/r2_4o4.png'); //unique to each round
        game.load.image('r2_4o4r', 'assets/r2_4o4r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_2_4 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_2_4');
        this.player1_2_4.scale.x = 0.083;
        this.player1_2_4.scale.y = 0.162;

        this.player2_2_4 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_2_4');
        this.player2_2_4.scale.x = 0.083;
        this.player2_2_4.scale.y = 0.162;

        this.correct_bar_1_p1_2_4 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1000 * 202));
        this.correct_bar_1_p2_2_4 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));

        this.q4 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q4'); //don't forget to modify!!!
        this.q4.scale.x = 0.538;
        this.q4.scale.y = 0.55;

        this.bar1_2_4 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_2_4');
        this.bar1_2_4.scale.x = 0.3;
        this.bar1_2_4.scale.y = 0.415;

        this.bar2_2_4 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_2_4');
        this.bar2_2_4.scale.x = 0.3;
        this.bar2_2_4.scale.y = 0.415;

        this.timeInSeconds_OP_2_4 = 120;
        this.timer_OP_2_4 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_2_4--;
        var minutes_OP_2_4 = Math.floor(this.timeInSeconds_OP_2_4 / 10);
        var seconds_OP_2_4 = this.timeInSeconds_OP_2_4 - (minutes_OP_2_4 * 10);
    
        if(seconds_OP_2_4 == 8) {
            this.show_OP();
            this.q4.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_2_4);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_2_4--;
        var minutes_2_4 = Math.floor(this.timeInSeconds_2_4 / 10);
        var seconds_2_4 = this.timeInSeconds_2_4 - (minutes_2_4 * 10);
        var timeString_2_4 = this.addZeros(seconds_2_4);
        this.timeText_2_4.text = timeString_2_4;
    
        if(seconds_2_4 == 7 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_2_4 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_2_4, this);
            player2_answered = true;
        }

        if (seconds_2_4 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_2_4 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r4r = game.add.sprite(game.width/2-102, game.height/2+175.5, 'r2_4o4r');
                this.r4r.scale.x = 0.477;
                this.r4r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_4, this);
            }
            else {
                this.r4r = game.add.sprite(game.width/2-102, game.height/2+175.5, 'r2_4o4r');
                this.r4r.scale.x = 0.477;
                this.r4r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_4r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_2_4r() {
        game.time.events.remove(this.timer_2_4);
        this.timeText_2_4.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l2_5');
    },
    destroy_rectangle_p1_nextLevel_2_4() {
        this.wrong_bar_1_p1_2_4.width = 0;
        this.wrong_bar_1_p1_2_4.length = 0;
        game.time.events.remove(this.timer_2_4);
        this.timeText_2_4.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l2_5');
    },
    destroy_rectangle_p1_2_4() {
        this.wrong_bar_1_p1_2_4.width = 0;
        this.wrong_bar_1_p1_2_4.length = 0;
    },
    destroy_rectangle_p2_2_4() {
        this.wrong_bar_1_p2_2_4.width = 0;
        this.wrong_bar_1_p2_2_4.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_2_4 = 120;
        this.timeText_2_4 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_2_4.anchor.set(0.5, 0.5);
        this.timer_2_4 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question4 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question4'); //要改成第n題
        this.question4.scale.x = 0.2;
        this.question4.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q2_4o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_2_4, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q2_4o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_2_4, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q2_4o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_2_4, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q2_4o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_2_4, this);
    },
    clickOption1_2_4: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+7, 'w2_4o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_4 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_4, this);
        player1_answered = true;
    },
    clickOption2_2_4: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+63, 'w2_4o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_4 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_4, this);
        player1_answered = true;
    },
    clickOption3_2_4: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+119, 'w2_4o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_4 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_4, this);
        player1_answered = true;
    },
    clickOption4_2_4: function() {
        this.r4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'r2_4o4');
        this.r4.scale.x = 0.477;
        this.r4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_2_4 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_2_4, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_2_4, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_2_4, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_2_4, '#33FFC6');
    }
};
var l2_5 = {
    preload: function() {
        game.load.image('player1_2_5', 'assets/l2_p1.jpg');
        game.load.image('player2_2_5', 'assets/l2_p2.jpg');
        game.load.image('bar_2_5', 'assets/bar.png');
        game.load.image('option_2_5', 'assets/option.png');
        game.load.image('q2_5o1', 'assets/q2_5o1.png'); //unique to each round
        game.load.image('q2_5o2', 'assets/q2_5o2.png'); //unique to each round
        game.load.image('q2_5o3', 'assets/q2_5o3.png'); //unique to each round
        game.load.image('q2_5o4', 'assets/q2_5o4.png'); //unique to each round
        game.load.image('gaming_2_5', 'assets/game_situation.jpg');
        game.load.image('q5', 'assets/last_question.png'); 
        game.load.image('firstq', 'assets/l2_background.jpg'); //background
        game.load.image('question5', 'assets/2_5.png'); 
        game.load.image('r2_5o1', 'assets/r2_5o1.png'); //unique to each round
        game.load.image('w2_5o2', 'assets/w2_5o2.png'); //unique to each round
        game.load.image('w2_5o3', 'assets/w2_5o3.png'); //unique to each round
        game.load.image('w2_5o4', 'assets/w2_5o4.png'); //unique to each round
        game.load.image('r2_5o1r', 'assets/r2_5o1r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_2_5 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_2_5');
        this.player1_2_5.scale.x = 0.083;
        this.player1_2_5.scale.y = 0.162;

        this.player2_2_5 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_2_5');
        this.player2_2_5.scale.x = 0.083;
        this.player2_2_5.scale.y = 0.162;

        this.correct_bar_1_p1_2_5 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1000 * 202));
        this.correct_bar_1_p2_2_5 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));

        this.q5 = game.add.sprite(game.width/2 - 125, game.height/2 - 127, 'q5'); //don't forget to modify!!!
        this.q5.scale.x = 0.538;
        this.q5.scale.y = 0.55;

        this.bar1_2_5 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_2_5');
        this.bar1_2_5.scale.x = 0.3;
        this.bar1_2_5.scale.y = 0.415;

        this.bar2_2_5 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_2_5');
        this.bar2_2_5.scale.x = 0.3;
        this.bar2_2_5.scale.y = 0.415;

        this.timeInSeconds_OP_2_5 = 120;
        this.timer_OP_2_5 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_2_5--;
        var minutes_OP_2_5 = Math.floor(this.timeInSeconds_OP_2_5 / 10);
        var seconds_OP_2_5 = this.timeInSeconds_OP_2_5 - (minutes_OP_2_5 * 10);
    
        if(seconds_OP_2_5 == 8) {
            this.show_OP();
            this.q5.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_2_5);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_2_5--;
        var minutes_2_5 = Math.floor(this.timeInSeconds_2_5 / 10);
        var seconds_2_5 = this.timeInSeconds_2_5 - (minutes_2_5 * 10);
        var timeString_2_5 = this.addZeros(seconds_2_5);
        this.timeText_2_5.text = timeString_2_5;
    
        if(seconds_2_5 == 5 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_2_5 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1000 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_1_4, this);
            player2_answered = true;
        }

        if (seconds_2_5 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_2_5 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
                this.r1r = game.add.sprite(game.width/2-102, game.height/2+119, 'r2_5o1r');
                this.r1r.scale.x = 0.477;
                this.r1r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_5, this);
            }
            else {
                this.r1r = game.add.sprite(game.width/2-102, game.height/2+7, 'r2_5o1r');
                this.r1r.scale.x = 0.477;
                this.r1r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_2_5r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_2_5r() {
        game.time.events.remove(this.timer_2_5);
        this.timeText_2_5.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('round_over');
    },
    destroy_rectangle_p1_nextLevel_2_5() {
        this.wrong_bar_1_p1_2_5.width = 0;
        this.wrong_bar_1_p1_2_5.length = 0;
        game.time.events.remove(this.timer_2_5);
        this.timeText_2_5.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('round_over');
    },
    destroy_rectangle_p1_2_5() {
        this.wrong_bar_1_p1_2_5.width = 0;
        this.wrong_bar_1_p1_2_5.length = 0;
    },
    destroy_rectangle_p2_2_5() {
        this.wrong_bar_1_p2_2_5.width = 0;
        this.wrong_bar_1_p2_2_5.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_2_5 = 120;
        this.timeText_2_5 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_2_5.anchor.set(0.5, 0.5);
        this.timer_2_5 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question5 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question5'); //要改成第n題
        this.question5.scale.x = 0.2;
        this.question5.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q2_5o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_2_5, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q2_5o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_2_5, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q2_5o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_2_5, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q2_5o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_2_5, this);
    },
    clickOption1_2_5: function() {
        this.r1 = game.add.sprite(game.width/2-102, game.height/2+7, 'r2_5o1');
        this.r1.scale.x = 0.477;
        this.r1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_2_5 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1000 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption2_2_5: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+63, 'w2_5o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_5 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_5, this);
        player1_answered = true;
    },
    clickOption3_2_5: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+119, 'w2_5o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_5 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_5, this);
        player1_answered = true;
    },
    clickOption4_2_5: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'w2_5o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_2_5 = new Phaser.Rectangle(14.5, 288, 10, (1000 - player1_score) / 1000 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_2_5, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_2_5, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_2_5, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_2_5, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_2_5, '#33FFC6');
    }
}
var l3_1 = {
    preload: function() {
        game.load.image('player1_3_1', 'assets/l3_p1.jpg');
        game.load.image('player2_3_1', 'assets/l3_p2.jpg');
        game.load.image('bar_3_1', 'assets/bar.png');
        game.load.image('option_3_1', 'assets/option.png');
        game.load.image('q3_1o1', 'assets/q3_1o1.png'); //unique to each round
        game.load.image('q3_1o2', 'assets/q3_1o2.png'); //unique to each round
        game.load.image('q3_1o3', 'assets/q3_1o3.png'); //unique to each round
        game.load.image('q3_1o4', 'assets/q3_1o4.png'); //unique to each round
        game.load.image('gaming_3_1', 'assets/game_situation.jpg');
        game.load.image('q1', 'assets/question_1.png'); //unique to each round
        game.load.image('firstq', 'assets/l3_background.jpg'); //unique to each round
        game.load.image('question1', 'assets/3_1.png'); //unique to each round
        game.load.image('r3_1o1', 'assets/r3_1o1.png'); //unique to each round
        game.load.image('w3_1o2', 'assets/w3_1o2.png'); //unique to each round
        game.load.image('w3_1o3', 'assets/w3_1o3.png'); //unique to each round
        game.load.image('w3_1o4', 'assets/w3_1o4.png'); //unique to each round
        game.load.image('r3_1o1r', 'assets/r3_1o1r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_3_1 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_3_1');
        this.player1_3_1.scale.x = 0.083;
        this.player1_3_1.scale.y = 0.162;

        this.player2_3_1 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_3_1');
        this.player2_3_1.scale.x = 0.083;
        this.player2_3_1.scale.y = 0.162;

        this.q1 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q1');
        this.q1.scale.x = 0.538;
        this.q1.scale.y = 0.55;

        this.bar1_3_1 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_3_1');
        this.bar1_3_1.scale.x = 0.3;
        this.bar1_3_1.scale.y = 0.415;

        this.bar2_3_1 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_3_1');
        this.bar2_3_1.scale.x = 0.3;
        this.bar2_3_1.scale.y = 0.415;

        this.timeInSeconds_OP_3_1 = 120;
        this.timer_OP_3_1 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_3_1--;
        var minutes_OP_3_1 = Math.floor(this.timeInSeconds_OP_3_1 / 10);
        var seconds_OP_3_1 = this.timeInSeconds_OP_3_1 - (minutes_OP_3_1 * 10);
    
        if(seconds_OP_3_1 == 8) {
            this.show_OP();
            this.q1.kill();
            game.time.events.remove(this.timer_OP_3_1);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_3_1--;
        var minutes_3_1 = Math.floor(this.timeInSeconds_3_1 / 10);
        var seconds_3_1 = this.timeInSeconds_3_1 - (minutes_3_1 * 10);
        var timeString_3_1 = this.addZeros(seconds_3_1);
        this.timeText_3_1.text = timeString_3_1;
    
        if(seconds_3_1 == 5 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_3_1 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_3_1, this);
            player2_answered = true;
        }

        if (seconds_3_1 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_3_1 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
                this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r3_1o1r');
                this.r1.scale.x = 0.477;
                this.r1.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_1, this);
            }
            else {
                this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r3_1o1r');
                this.r1.scale.x = 0.477;
                this.r1.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_1r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_3_1r() {
        game.time.events.remove(this.timer_3_1);
        this.timeText_3_1.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_2');
    },
    destroy_rectangle_p1_nextLevel_3_1() {
        this.wrong_bar_1_p1_3_1.width = 0;
        this.wrong_bar_1_p1_3_1.length = 0;
        game.time.events.remove(this.timer_3_1);
        this.timeText_3_1.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_2');
    },
    destroy_rectangle_p1_3_1() {
        this.wrong_bar_1_p1_3_1.width = 0;
        this.wrong_bar_1_p1_3_1.length = 0;
    },
    destroy_rectangle_p2_3_1() {
        this.wrong_bar_1_p2_3_1.width = 0;
        this.wrong_bar_1_p2_3_1.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_3_1 = 120;
        this.timeText_3_1 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_3_1.anchor.set(0.5, 0.5);
        this.timer_3_1 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question1 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question1');
        this.question1.scale.x = 0.2;
        this.question1.scale.y = 0.215;

        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q3_1o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_3_1, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q3_1o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_3_1, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q3_1o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_3_1, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q3_1o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_3_1, this);
    },
    clickOption1_3_1: function() {
        this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r3_1o1');
        this.r1.scale.x = 0.477;
        this.r1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.correct_bar_1_p1_3_1 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1200 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption2_3_1: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+61, 'w3_1o2');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_1 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_1, this);
        player1_answered = true;
    },
    clickOption3_3_1: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+117, 'w3_1o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_1 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_1, this);
        player1_answered = true;
    },
    clickOption4_3_1: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'w3_1o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_1 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_1, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_3_1, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_3_1, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_3_1, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_3_1, '#33FFC6');
    }
};
var l3_2 = {
    preload: function() {
        game.load.image('player1_3_2', 'assets/l3_p1.jpg');
        game.load.image('player2_3_2', 'assets/l3_p2.jpg');
        game.load.image('bar_3_2', 'assets/bar.png');
        game.load.image('option_3_2', 'assets/option.png');
        game.load.image('q3_2o1', 'assets/q3_2o1.png'); //unique to each round
        game.load.image('q3_2o2', 'assets/q3_2o2.png'); //unique to each round
        game.load.image('q3_2o3', 'assets/q3_2o3.png'); //unique to each round
        game.load.image('q3_2o4', 'assets/q3_2o4.png'); //unique to each round
        game.load.image('gaming_3_2', 'assets/game_situation.jpg');
        game.load.image('q2', 'assets/question_2.png'); //unique to each round
        game.load.image('firstq', 'assets/l3_background.jpg'); //background
        game.load.image('question2', 'assets/3_2.png'); //unique to each round
        game.load.image('w3_2o1', 'assets/w3_2o1.png'); //unique to each round
        game.load.image('r3_2o2', 'assets/r3_2o2.png'); //unique to each round
        game.load.image('w3_2o3', 'assets/w3_2o3.png'); //unique to each round
        game.load.image('w3_2o4', 'assets/w3_2o4.png'); //unique to each round
        game.load.image('r3_2o2r', 'assets/r3_2o2r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_3_2 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_3_2');
        this.player1_3_2.scale.x = 0.083;
        this.player1_3_2.scale.y = 0.162;

        this.player2_3_2 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_3_2');
        this.player2_3_2.scale.x = 0.083;
        this.player2_3_2.scale.y = 0.162;

        this.correct_bar_1_p1_3_2 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1200 * 202));
        this.correct_bar_1_p2_3_2 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));

        this.q2 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q2');
        this.q2.scale.x = 0.538;
        this.q2.scale.y = 0.55;

        this.bar1_3_2 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_3_2');
        this.bar1_3_2.scale.x = 0.3;
        this.bar1_3_2.scale.y = 0.415;

        this.bar2_3_2 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_3_2');
        this.bar2_3_2.scale.x = 0.3;
        this.bar2_3_2.scale.y = 0.415;

        this.timeInSeconds_OP_3_2 = 120;
        this.timer_OP_3_2 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_3_2--;
        var minutes_OP_3_2 = Math.floor(this.timeInSeconds_OP_3_2 / 10);
        var seconds_OP_3_2 = this.timeInSeconds_OP_3_2 - (minutes_OP_3_2 * 10);
    
        if(seconds_OP_3_2 == 8) {
            this.show_OP();
            this.q2.kill();
            game.time.events.remove(this.timer_OP_3_2);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_3_2--;
        var minutes_3_2 = Math.floor(this.timeInSeconds_3_2 / 10);
        var seconds_3_2 = this.timeInSeconds_3_2 - (minutes_3_2 * 10);
        var timeString_3_2 = this.addZeros(seconds_3_2);
        this.timeText_3_2.text = timeString_3_2;
    
        if(seconds_3_2 == 2 && !destroyed) {
            this.wrong_bar_1_p2_3_2 = new Phaser.Rectangle(285, 288, 10, (1200 - player2_score) / 1200 * 202);
            this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_3_2, this);
            player2_answered = true;
        }

        if (seconds_3_2 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_3_2 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+61, 'r3_2o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_2, this);
            }
            else {
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+61, 'r3_2o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_2r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_3_2r() {
        game.time.events.remove(this.timer_3_2);
        this.timeText_3_2.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_3');
    },
    destroy_rectangle_p1_nextLevel_3_2() {
        this.wrong_bar_1_p1_3_2.width = 0;
        this.wrong_bar_1_p1_3_2.length = 0;
        game.time.events.remove(this.timer_3_2);
        this.timeText_3_2.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_3');
    },
    destroy_rectangle_p1_3_2() {
        this.wrong_bar_1_p1_3_2.width = 0;
        this.wrong_bar_1_p1_3_2.length = 0;
    },
    destroy_rectangle_p2_3_2() {
        this.wrong_bar_1_p2_3_2.width = 0;
        this.wrong_bar_1_p2_3_2.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_3_2 = 120;
        this.timeText_3_2 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_3_2.anchor.set(0.5, 0.5);
        this.timer_3_2 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question2 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question2'); //要改成第n題
        this.question2.scale.x = 0.2;
        this.question2.scale.y = 0.215;

        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q3_2o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_3_2, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q3_2o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_3_2, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q3_2o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_3_2, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q3_2o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_3_2, this);
    },
    clickOption1_3_2: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+5, 'w3_2o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_2 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_2, this);
        player1_answered = true;
    },
    clickOption2_3_2: function() {
        this.r2 = game.add.sprite(game.width/2-102, game.height/2+61, 'r3_2o2');
        this.r2.scale.x = 0.477;
        this.r2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_3_2 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1200 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption3_3_2: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+117, 'w3_2o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_2 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_2, this);
        player1_answered = true;
    },
    clickOption4_3_2: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'w3_2o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_2 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_2, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_3_2, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_3_2, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_3_2, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_3_2, '#33FFC6');
    }
};
var l3_3 = {
    preload: function() {
        game.load.image('player1_3_3', 'assets/l3_p1.jpg');
        game.load.image('player2_3_3', 'assets/l3_p2.jpg');
        game.load.image('bar_3_3', 'assets/bar.png');
        game.load.image('option_3_3', 'assets/option.png');
        game.load.image('q3_3o1', 'assets/q3_3o1.png'); //unique to each round
        game.load.image('q3_3o2', 'assets/q3_3o2.png'); //unique to each round
        game.load.image('q3_3o3', 'assets/q3_3o3.png'); //unique to each round
        game.load.image('q3_3o4', 'assets/q3_3o4.png'); //unique to each round
        game.load.image('gaming_3_3', 'assets/game_situation.jpg');
        game.load.image('q3', 'assets/question_3.png'); //unique to each round
        game.load.image('firstq', 'assets/l3_background.jpg'); //background
        game.load.image('question3', 'assets/3_3.png'); //unique to each round
        game.load.image('w3_3o1', 'assets/w3_3o1.png'); //unique to each round
        game.load.image('r3_3o2', 'assets/r3_3o2.png'); //unique to each round
        game.load.image('w3_3o3', 'assets/w3_3o3.png'); //unique to each round
        game.load.image('w3_3o4', 'assets/w3_3o4.png'); //unique to each round
        game.load.image('r3_3o2r', 'assets/r3_3o2r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_3_3 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_3_3');
        this.player1_3_3.scale.x = 0.083;
        this.player1_3_3.scale.y = 0.162;

        this.player2_3_3 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_3_3');
        this.player2_3_3.scale.x = 0.083;
        this.player2_3_3.scale.y = 0.162;

        this.correct_bar_1_p1_3_3 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1200 * 202));
        this.correct_bar_1_p2_3_3 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));

        this.q3 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q3'); //don't forget to modify!!!
        this.q3.scale.x = 0.538;
        this.q3.scale.y = 0.55;

        this.bar1_3_3 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_3_3');
        this.bar1_3_3.scale.x = 0.3;
        this.bar1_3_3.scale.y = 0.415;

        this.bar2_3_3 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_3_3');
        this.bar2_3_3.scale.x = 0.3;
        this.bar2_3_3.scale.y = 0.415;

        this.timeInSeconds_OP_3_3 = 120;
        this.timer_OP_3_3 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_3_3--;
        var minutes_OP_3_3 = Math.floor(this.timeInSeconds_OP_3_3 / 10);
        var seconds_OP_3_3 = this.timeInSeconds_OP_3_3 - (minutes_OP_3_3 * 10);
    
        if(seconds_OP_3_3 == 8) {
            this.show_OP();
            this.q3.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_3_3);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_3_3--;
        var minutes_3_3 = Math.floor(this.timeInSeconds_3_3 / 10);
        var seconds_3_3 = this.timeInSeconds_3_3 - (minutes_3_3 * 10);
        var timeString_3_3 = this.addZeros(seconds_3_3);
        this.timeText_3_3.text = timeString_3_3;
    
        if(seconds_3_3 == 8 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_3_3 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_3_3, this);
            player2_answered = true;
        }

        if (seconds_3_3 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_3_3 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+61, 'r3_3o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_3, this);
            }
            else {
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+61, 'r3_3o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_3r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_3_3r() {
        game.time.events.remove(this.timer_3_3);
        this.timeText_3_3.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_4');
    },
    destroy_rectangle_p1_nextLevel_3_3() {
        this.wrong_bar_1_p1_3_3.width = 0;
        this.wrong_bar_1_p1_3_3.length = 0;
        game.time.events.remove(this.timer_3_3);
        this.timeText_3_3.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_4');
    },
    destroy_rectangle_p1_3_3() {
        this.wrong_bar_1_p1_3_3.width = 0;
        this.wrong_bar_1_p1_3_3.length = 0;
    },
    destroy_rectangle_p2_3_3() {
        this.wrong_bar_1_p2_3_3.width = 0;
        this.wrong_bar_1_p2_3_3.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_3_3 = 120;
        this.timeText_3_3 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_3_3.anchor.set(0.5, 0.5);
        this.timer_3_3 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question3 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question3'); //要改成第n題
        this.question3.scale.x = 0.2;
        this.question3.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q3_3o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_3_3, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q3_3o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_3_3, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q3_3o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_3_3, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q3_3o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_3_3, this);
    },
    clickOption1_3_3: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+5, 'w3_3o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_3 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_3, this);
        player1_answered = true;
    },
    clickOption2_3_3: function() {
        this.r2 = game.add.sprite(game.width/2-102, game.height/2+61, 'r3_3o2');
        this.r2.scale.x = 0.477;
        this.r2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.correct_bar_1_p1_3_3 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1200 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption3_3_3: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+117, 'w3_3o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_3 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_3, this);
        player1_answered = true;
    },
    clickOption4_3_3: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'w3_3o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_3 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_3, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_3_3, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_3_3, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_3_3, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_3_3, '#33FFC6');
    }
};
var l3_4 = {
    preload: function() {
        game.load.image('player1_3_4', 'assets/l3_p1.jpg');
        game.load.image('player2_3_4', 'assets/l3_p2.jpg');
        game.load.image('bar_3_4', 'assets/bar.png');
        game.load.image('option_3_4', 'assets/option.png');
        game.load.image('q3_4o1', 'assets/q3_4o1.png'); //unique to each round
        game.load.image('q3_4o2', 'assets/q3_4o2.png'); //unique to each round
        game.load.image('q3_4o3', 'assets/q3_4o3.png'); //unique to each round
        game.load.image('q3_4o4', 'assets/q3_4o4.png'); //unique to each round
        game.load.image('gaming_3_4', 'assets/game_situation.jpg');
        game.load.image('q4', 'assets/question_4.png'); //unique to each round
        game.load.image('firstq', 'assets/l3_background.jpg'); //background
        game.load.image('question4', 'assets/3_4.png'); //unique to each round
        game.load.image('w3_4o1', 'assets/w3_4o1.png'); //unique to each round
        game.load.image('w3_4o2', 'assets/w3_4o2.png'); //unique to each round
        game.load.image('w3_4o3', 'assets/w3_4o3.png'); //unique to each round
        game.load.image('r3_4o4', 'assets/r3_4o4.png'); //unique to each round
        game.load.image('r3_4o4r', 'assets/r3_4o4r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_3_4 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_3_4');
        this.player1_3_4.scale.x = 0.083;
        this.player1_3_4.scale.y = 0.162;

        this.player2_3_4 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_3_4');
        this.player2_3_4.scale.x = 0.083;
        this.player2_3_4.scale.y = 0.162;

        this.correct_bar_1_p1_3_4 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1200 * 202));
        this.correct_bar_1_p2_3_4 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));

        this.q4 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q4'); //don't forget to modify!!!
        this.q4.scale.x = 0.538;
        this.q4.scale.y = 0.55;

        this.bar1_3_4 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_3_4');
        this.bar1_3_4.scale.x = 0.3;
        this.bar1_3_4.scale.y = 0.415;

        this.bar2_3_4 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_3_4');
        this.bar2_3_4.scale.x = 0.3;
        this.bar2_3_4.scale.y = 0.415;

        this.timeInSeconds_OP_3_4 = 120;
        this.timer_OP_3_4 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_3_4--;
        var minutes_OP_3_4 = Math.floor(this.timeInSeconds_OP_3_4 / 10);
        var seconds_OP_3_4 = this.timeInSeconds_OP_3_4 - (minutes_OP_3_4 * 10);
    
        if(seconds_OP_3_4 == 8) {
            this.show_OP();
            this.q4.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_3_4);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_3_4--;
        var minutes_3_4 = Math.floor(this.timeInSeconds_3_4 / 10);
        var seconds_3_4 = this.timeInSeconds_3_4 - (minutes_3_4 * 10);
        var timeString_3_4 = this.addZeros(seconds_3_4);
        this.timeText_3_4.text = timeString_3_4;
    
        if(seconds_3_4 == 7 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_3_4 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_3_4, this);
            player2_answered = true;
        }

        if (seconds_3_4 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_3_4 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
                this.r4r = game.add.sprite(game.width/2-102, game.height/2+173.5, 'r3_4o4r');
                this.r4r.scale.x = 0.477;
                this.r4r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_4, this);
            }
            else {
                this.r4r = game.add.sprite(game.width/2-102, game.height/2+173.5, 'r3_4o4r');
                this.r4r.scale.x = 0.477;
                this.r4r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_4r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_3_4r() {
        game.time.events.remove(this.timer_3_4);
        this.timeText_3_4.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_5');
    },
    destroy_rectangle_p1_nextLevel_3_4() {
        this.wrong_bar_1_p1_3_4.width = 0;
        this.wrong_bar_1_p1_3_4.length = 0;
        game.time.events.remove(this.timer_3_4);
        this.timeText_3_4.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_5');
    },
    destroy_rectangle_p1_3_4() {
        this.wrong_bar_1_p1_3_4.width = 0;
        this.wrong_bar_1_p1_3_4.length = 0;
    },
    destroy_rectangle_p2_3_4() {
        this.wrong_bar_1_p2_3_4.width = 0;
        this.wrong_bar_1_p2_3_4.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_3_4 = 120;
        this.timeText_3_4 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_3_4.anchor.set(0.5, 0.5);
        this.timer_3_4 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question4 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question4'); //要改成第n題
        this.question4.scale.x = 0.2;
        this.question4.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q3_4o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_3_4, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q3_4o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_3_4, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q3_4o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_3_4, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q3_4o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_3_4, this);
    },
    clickOption1_3_4: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+5, 'w3_4o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_4 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_4, this);
        player1_answered = true;
    },
    clickOption2_3_4: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+61, 'w3_4o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_4 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_4, this);
        player1_answered = true;
    },
    clickOption3_3_4: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+117, 'w3_4o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_4 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_4, this);
        player1_answered = true;
    },
    clickOption4_3_4: function() {
        this.r4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'r3_4o4');
        this.r4.scale.x = 0.477;
        this.r4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_3_4 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1200 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_3_4, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_3_4, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_3_4, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_3_4, '#33FFC6');
    }
};
var l3_5 = {
    preload: function() {
        game.load.image('player1_3_5', 'assets/l3_p1.jpg');
        game.load.image('player2_3_5', 'assets/l3_p2.jpg');
        game.load.image('bar_3_5', 'assets/bar.png');
        game.load.image('option_3_5', 'assets/option.png');
        game.load.image('q3_5o1', 'assets/q3_5o1.png'); //unique to each round
        game.load.image('q3_5o2', 'assets/q3_5o2.png'); //unique to each round
        game.load.image('q3_5o3', 'assets/q3_5o3.png'); //unique to each round
        game.load.image('q3_5o4', 'assets/q3_5o4.png'); //unique to each round
        game.load.image('gaming_3_5', 'assets/game_situation.jpg');
        game.load.image('q5', 'assets/question_5.png'); 
        game.load.image('firstq', 'assets/l3_background.jpg'); //background
        game.load.image('question5', 'assets/3_5.png'); 
        game.load.image('w3_5o1', 'assets/w3_5o1.png'); //unique to each round
        game.load.image('w3_5o2', 'assets/w3_5o2.png'); //unique to each round
        game.load.image('w3_5o3', 'assets/w3_5o3.png'); //unique to each round
        game.load.image('r3_5o4', 'assets/r3_5o4.png'); //unique to each round
        game.load.image('r3_5o4r', 'assets/r3_5o4r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_3_5 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_3_5');
        this.player1_3_5.scale.x = 0.083;
        this.player1_3_5.scale.y = 0.162;

        this.player2_3_5 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_3_5');
        this.player2_3_5.scale.x = 0.083;
        this.player2_3_5.scale.y = 0.162;

        this.correct_bar_1_p1_3_5 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1200 * 202));
        this.correct_bar_1_p2_3_5 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));

        this.q5 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q5'); //don't forget to modify!!!
        this.q5.scale.x = 0.538;
        this.q5.scale.y = 0.55;

        this.bar1_3_5 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_3_5');
        this.bar1_3_5.scale.x = 0.3;
        this.bar1_3_5.scale.y = 0.415;

        this.bar2_3_5 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_3_5');
        this.bar2_3_5.scale.x = 0.3;
        this.bar2_3_5.scale.y = 0.415;

        this.timeInSeconds_OP_3_5 = 120;
        this.timer_OP_3_5 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_3_5--;
        var minutes_OP_3_5 = Math.floor(this.timeInSeconds_OP_3_5 / 10);
        var seconds_OP_3_5 = this.timeInSeconds_OP_3_5 - (minutes_OP_3_5 * 10);
    
        if(seconds_OP_3_5 == 8) {
            this.show_OP();
            this.q5.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_3_5);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_3_5--;
        var minutes_3_5 = Math.floor(this.timeInSeconds_3_5 / 10);
        var seconds_3_5 = this.timeInSeconds_3_5 - (minutes_3_5 * 10);
        var timeString_3_5 = this.addZeros(seconds_3_5);
        this.timeText_3_5.text = timeString_3_5;
    
        if(seconds_3_5 == 5 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_3_5 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_1_4, this);
            player2_answered = true;
        }

        if (seconds_3_5 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_3_5 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
                this.r4r = game.add.sprite(game.width/2-102, game.height/2+173.5, 'r3_5o4r');
                this.r4r.scale.x = 0.477;
                this.r4r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_5, this);
            }
            else {
                this.r4r = game.add.sprite(game.width/2-102, game.height/2+173.5, 'r3_5o4r');
                this.r4r.scale.x = 0.477;
                this.r4r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_5r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_3_5r() {
        game.time.events.remove(this.timer_3_5);
        this.timeText_3_5.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_6');
    },
    destroy_rectangle_p1_nextLevel_3_5() {
        this.wrong_bar_1_p1_3_5.width = 0;
        this.wrong_bar_1_p1_3_5.length = 0;
        game.time.events.remove(this.timer_3_5);
        this.timeText_3_5.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l3_6');
    },
    destroy_rectangle_p1_3_5() {
        this.wrong_bar_1_p1_3_5.width = 0;
        this.wrong_bar_1_p1_3_5.length = 0;
    },
    destroy_rectangle_p2_3_5() {
        this.wrong_bar_1_p2_3_5.width = 0;
        this.wrong_bar_1_p2_3_5.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_3_5 = 120;
        this.timeText_3_5 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_3_5.anchor.set(0.5, 0.5);
        this.timer_3_5 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question5 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question5'); //要改成第n題
        this.question5.scale.x = 0.2;
        this.question5.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q3_5o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_3_5, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q3_5o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_3_5, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q3_5o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_3_5, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q3_5o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_3_5, this);
    },
    clickOption1_3_5: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+5, 'w3_5o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_5 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_5, this);
        player1_answered = true;
    },
    clickOption2_3_5: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+61, 'w3_5o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_5 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_5, this);
        player1_answered = true;
    },
    clickOption3_3_5: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+116, 'w3_5o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_5 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_5, this);
        player1_answered = true;
    },
    clickOption4_3_5: function() {
        this.r4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'r3_5o4');
        this.r4.scale.x = 0.477;
        this.r4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_3_5 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1200 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_3_5, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_3_5, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_3_5, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_3_5, '#33FFC6');
    }
}
var l3_6 = {
    preload: function() {
        game.load.image('player1_3_6', 'assets/l3_p1.jpg');
        game.load.image('player2_3_6', 'assets/l3_p2.jpg');
        game.load.image('bar_3_6', 'assets/bar.png');
        game.load.image('option_36', 'assets/option.png');
        game.load.image('q3_6o1', 'assets/q3_6o1.png'); //unique to each round
        game.load.image('q3_6o2', 'assets/q3_6o2.png'); //unique to each round
        game.load.image('q3_6o3', 'assets/q3_6o3.png'); //unique to each round
        game.load.image('q3_6o4', 'assets/q3_6o4.png'); //unique to each round
        game.load.image('gaming_3_6', 'assets/game_situation.jpg');
        game.load.image('q6', 'assets/last_question.png'); 
        game.load.image('firstq', 'assets/l3_background.jpg'); //background
        game.load.image('question6', 'assets/3_6.png'); 
        game.load.image('w3_6o1', 'assets/w3_6o1.png'); //unique to each round
        game.load.image('w3_6o2', 'assets/w3_6o2.png'); //unique to each round
        game.load.image('r3_6o3', 'assets/r3_6o3.png'); //unique to each round
        game.load.image('w3_6o4', 'assets/w3_6o4.png'); //unique to each round
        game.load.image('r3_6o3r', 'assets/r3_6o3r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_3_6 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_3_6');
        this.player1_3_6.scale.x = 0.083;
        this.player1_3_6.scale.y = 0.162;

        this.player2_3_6 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_3_6');
        this.player2_3_6.scale.x = 0.083;
        this.player2_3_6.scale.y = 0.162;

        this.correct_bar_1_p1_3_6 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 1200 * 202));
        this.correct_bar_1_p2_3_6 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));

        this.q6 = game.add.sprite(game.width/2 - 125, game.height/2 - 127, 'q6'); //don't forget to modify!!!
        this.q6.scale.x = 0.538;
        this.q6.scale.y = 0.55;

        this.bar1_3_6 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_3_6');
        this.bar1_3_6.scale.x = 0.3;
        this.bar1_3_6.scale.y = 0.415;

        this.bar2_3_6 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_3_6');
        this.bar2_3_6.scale.x = 0.3;
        this.bar2_3_6.scale.y = 0.415;

        this.timeInSeconds_OP_3_6 = 120;
        this.timer_OP_3_6 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_3_6--;
        var minutes_OP_3_6 = Math.floor(this.timeInSeconds_OP_3_6 / 10);
        var seconds_OP_3_6 = this.timeInSeconds_OP_3_6 - (minutes_OP_3_6 * 10);
    
        if(seconds_OP_3_6 == 8) {
            this.show_OP();
            this.q6.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_3_6);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_3_6--;
        var minutes_3_6 = Math.floor(this.timeInSeconds_3_6 / 10);
        var seconds_3_6 = this.timeInSeconds_3_6 - (minutes_3_6 * 10);
        var timeString_3_6 = this.addZeros(seconds_3_6);
        this.timeText_3_6.text = timeString_3_6;
    
        if(seconds_3_6 == 1 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_3_6 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 1200 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_1_4, this);
            player2_answered = true;
        }

        if (seconds_3_6 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_3_6 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
                this.r3r = game.add.sprite(game.width/2-102, game.height/2+117, 'r3_6o3r');
                this.r3r.scale.x = 0.477;
                this.r3r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_6, this);
            }
            else {
                this.r3r = game.add.sprite(game.width/2-102, game.height/2+117, 'r3_6o3r');
                this.r3r.scale.x = 0.477;
                this.r3r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_3_6r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_3_6r() {
        game.time.events.remove(this.timer_3_6);
        this.timeText_3_6.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('round_over');
    },
    destroy_rectangle_p1_nextLevel_3_6() {
        this.wrong_bar_1_p1_3_6.width = 0;
        this.wrong_bar_1_p1_3_6.length = 0;
        game.time.events.remove(this.timer_3_6);
        this.timeText_3_6.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('round_over');
    },
    destroy_rectangle_p1_3_6() {
        this.wrong_bar_1_p1_3_6.width = 0;
        this.wrong_bar_1_p1_3_6.length = 0;
    },
    destroy_rectangle_p2_3_6() {
        this.wrong_bar_1_p2_3_6.width = 0;
        this.wrong_bar_1_p2_3_6.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_3_6 = 120;
        this.timeText_3_6 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_3_6.anchor.set(0.5, 0.5);
        this.timer_3_6 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question6 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question6'); //要改成第n題
        this.question6.scale.x = 0.2;
        this.question6.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+5, 'q3_6o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_3_6, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q3_6o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_3_6, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q3_6o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_3_6, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q3_6o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_3_6, this);
    },
    clickOption1_3_6: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+5, 'w3_6o1');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_6 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_6, this);
        player1_answered = true;
    },
    clickOption2_3_6: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+61, 'w3_6o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_6 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_6, this);
        player1_answered = true;
    },
    clickOption3_3_6: function() {
        this.r3 = game.add.sprite(game.width/2-102, game.height/2+117, 'r3_6o3');
        this.r3.scale.x = 0.477;
        this.r3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_3_6 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 1200 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption4_3_6: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'w3_6o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_3_6 = new Phaser.Rectangle(14.5, 288, 10, (1200 - player1_score) / 1200 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_3_6, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_3_6, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_3_6, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_3_6, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_3_6, '#33FFC6');
    }
};
var l4_1 = {
    preload: function() {
        game.load.image('player1_4_1', 'assets/l4_p1.jpg');
        game.load.image('player2_4_1', 'assets/l4_p2.jpg');
        game.load.image('bar_4_1', 'assets/bar.png');
        game.load.image('option_4_1', 'assets/option.png');
        game.load.image('q4_1o1', 'assets/q4_1o1.png'); //unique to each round
        game.load.image('q4_1o2', 'assets/q4_1o2.png'); //unique to each round
        game.load.image('q4_1o3', 'assets/q4_1o3.png'); //unique to each round
        game.load.image('q4_1o4', 'assets/q4_1o4.png'); //unique to each round
        game.load.image('gaming_4_1', 'assets/game_situation.jpg');
        game.load.image('q1', 'assets/question_1.png'); //unique to each round
        game.load.image('firstq', 'assets/l4_background.jpg'); //unique to each round
        game.load.image('question1', 'assets/4_1.png'); //unique to each round
        game.load.image('w4_1o1', 'assets/w4_1o1.png'); //unique to each round
        game.load.image('w4_1o2', 'assets/w4_1o2.png'); //unique to each round
        game.load.image('r4_1o3', 'assets/r4_1o3.png'); //unique to each round
        game.load.image('w4_1o4', 'assets/w4_1o4.png'); //unique to each round
        game.load.image('r4_1o3r', 'assets/r4_1o3r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_4_1 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_4_1');
        this.player1_4_1.scale.x = 0.083;
        this.player1_4_1.scale.y = 0.162;

        this.player2_4_1 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_4_1');
        this.player2_4_1.scale.x = 0.083;
        this.player2_4_1.scale.y = 0.162;

        this.q1 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q1');
        this.q1.scale.x = 0.538;
        this.q1.scale.y = 0.55;

        this.bar1_4_1 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_4_1');
        this.bar1_4_1.scale.x = 0.3;
        this.bar1_4_1.scale.y = 0.415;

        this.bar2_4_1 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_4_1');
        this.bar2_4_1.scale.x = 0.3;
        this.bar2_4_1.scale.y = 0.415;

        this.timeInSeconds_OP_4_1 = 120;
        this.timer_OP_4_1 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_4_1--;
        var minutes_OP_4_1 = Math.floor(this.timeInSeconds_OP_4_1 / 10);
        var seconds_OP_4_1 = this.timeInSeconds_OP_4_1 - (minutes_OP_4_1 * 10);
    
        if(seconds_OP_4_1 == 8) {
            this.show_OP();
            this.q1.kill();
            game.time.events.remove(this.timer_OP_4_1);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_4_1--;
        var minutes_4_1 = Math.floor(this.timeInSeconds_4_1 / 10);
        var seconds_4_1 = this.timeInSeconds_4_1 - (minutes_4_1 * 10);
        var timeString_4_1 = this.addZeros(seconds_4_1);
        this.timeText_4_1.text = timeString_4_1;
    
        if(seconds_4_1 == 4 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_4_1 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 800 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_4_1, this);
            player2_answered = true;
        }

        if (seconds_4_1 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_4_1 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
                this.r3r = game.add.sprite(game.width/2-102, game.height/2+117, 'r4_1o3r');
                this.r3r.scale.x = 0.477;
                this.r3r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_4_1, this);
            }
            else {
                this.r3r = game.add.sprite(game.width/2-102, game.height/2+117, 'r4_1o3r');
                this.r3r.scale.x = 0.477;
                this.r3r.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_4_1r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_4_1r() {
        game.time.events.remove(this.timer_4_1);
        this.timeText_4_1.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l4_2');
    },
    destroy_rectangle_p1_nextLevel_4_1() {
        this.wrong_bar_1_p1_4_1.width = 0;
        this.wrong_bar_1_p1_4_1.length = 0;
        game.time.events.remove(this.timer_4_1);
        this.timeText_4_1.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l4_2');
    },
    destroy_rectangle_p1_4_1() {
        this.wrong_bar_1_p1_4_1.width = 0;
        this.wrong_bar_1_p1_4_1.length = 0;
    },
    destroy_rectangle_p2_4_1() {
        this.wrong_bar_1_p2_4_1.width = 0;
        this.wrong_bar_1_p2_4_1.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_4_1 = 120;
        this.timeText_4_1 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_4_1.anchor.set(0.5, 0.5);
        this.timer_4_1 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question1 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question1');
        this.question1.scale.x = 0.2;
        this.question1.scale.y = 0.215;

        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q4_1o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_4_1, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q4_1o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_4_1, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q4_1o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_4_1, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q4_1o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_4_1, this);
    },
    clickOption1_4_1: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+4.5, 'w4_1o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_1 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_1, this);
        player1_answered = true;
    },
    clickOption2_4_1: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+61, 'w4_1o2');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_1 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_1, this);
        player1_answered = true;
    },
    clickOption3_4_1: function() {
        this.r3 = game.add.sprite(game.width/2-102, game.height/2+117, 'r4_1o3');
        this.r3.scale.x = 0.477;
        this.r3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.correct_bar_1_p1_4_1 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 800 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption4_4_1: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'w4_1o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_1 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_1, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_4_1, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_4_1, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_4_1, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_4_1, '#33FFC6');
    }
};
var l4_2 = {
    preload: function() {
        game.load.image('player1_4_2', 'assets/l4_p1.jpg');
        game.load.image('player2_4_2', 'assets/l4_p2.jpg');
        game.load.image('bar_4_2', 'assets/bar.png');
        game.load.image('option_4_2', 'assets/option.png');
        game.load.image('q4_2o1', 'assets/q4_2o1.png'); //unique to each round
        game.load.image('q4_2o2', 'assets/q4_2o2.png'); //unique to each round
        game.load.image('q4_2o3', 'assets/q4_2o3.png'); //unique to each round
        game.load.image('q4_2o4', 'assets/q4_2o4.png'); //unique to each round
        game.load.image('gaming_4_2', 'assets/game_situation.jpg');
        game.load.image('q2', 'assets/question_2.png'); //unique to each round
        game.load.image('firstq', 'assets/l4_background.jpg'); //background
        game.load.image('question2', 'assets/4_2.png'); //unique to each round
        game.load.image('r4_2o1', 'assets/r4_2o1.png'); //unique to each round
        game.load.image('w4_2o2', 'assets/w4_2o2.png'); //unique to each round
        game.load.image('w4_2o3', 'assets/w4_2o3.png'); //unique to each round
        game.load.image('w4_2o4', 'assets/w4_2o4.png'); //unique to each round
        game.load.image('r4_2o1r', 'assets/r4_2o1r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_4_2 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_4_2');
        this.player1_4_2.scale.x = 0.083;
        this.player1_4_2.scale.y = 0.162;

        this.player2_4_2 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_4_2');
        this.player2_4_2.scale.x = 0.083;
        this.player2_4_2.scale.y = 0.162;

        this.correct_bar_1_p1_4_2 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 800 * 202));
        this.correct_bar_1_p2_4_2 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 800 * 202));

        this.q2 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q2');
        this.q2.scale.x = 0.538;
        this.q2.scale.y = 0.55;

        this.bar1_4_2 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_4_2');
        this.bar1_4_2.scale.x = 0.3;
        this.bar1_4_2.scale.y = 0.415;

        this.bar2_4_2 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_4_2');
        this.bar2_4_2.scale.x = 0.3;
        this.bar2_4_2.scale.y = 0.415;

        this.timeInSeconds_OP_4_2 = 120;
        this.timer_OP_4_2 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_4_2--;
        var minutes_OP_4_2 = Math.floor(this.timeInSeconds_OP_4_2 / 10);
        var seconds_OP_4_2 = this.timeInSeconds_OP_4_2 - (minutes_OP_4_2 * 10);
    
        if(seconds_OP_4_2 == 8) {
            this.show_OP();
            this.q2.kill();
            game.time.events.remove(this.timer_OP_4_2);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_4_2--;
        var minutes_4_2 = Math.floor(this.timeInSeconds_4_2 / 10);
        var seconds_4_2 = this.timeInSeconds_4_2 - (minutes_4_2 * 10);
        var timeString_4_2 = this.addZeros(seconds_4_2);
        this.timeText_4_2.text = timeString_4_2;
    
        if(seconds_4_2 == 2 && !destroyed) {
            this.wrong_bar_1_p2_4_2 = new Phaser.Rectangle(285, 288, 10, (800 - player2_score) / 800 * 202);
            this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_4_2, this);
            player2_answered = true;
        }

        if (seconds_4_2 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_4_2 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
                this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r4_2o1r');
                this.r1.scale.x = 0.477;
                this.r1.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_4_2, this);
            }
            else {
                this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r4_2o1r');
                this.r1.scale.x = 0.477;
                this.r1.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_4_2r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_4_2r() {
        game.time.events.remove(this.timer_4_2);
        this.timeText_4_2.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l4_3');
    },
    destroy_rectangle_p1_nextLevel_4_2() {
        this.wrong_bar_1_p1_4_2.width = 0;
        this.wrong_bar_1_p1_4_2.length = 0;
        game.time.events.remove(this.timer_4_2);
        this.timeText_4_2.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l4_3');
    },
    destroy_rectangle_p1_4_2() {
        this.wrong_bar_1_p1_4_2.width = 0;
        this.wrong_bar_1_p1_4_2.length = 0;
    },
    destroy_rectangle_p2_4_2() {
        this.wrong_bar_1_p2_4_2.width = 0;
        this.wrong_bar_1_p2_4_2.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_4_2 = 120;
        this.timeText_4_2 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_4_2.anchor.set(0.5, 0.5);
        this.timer_4_2 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question2 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question2'); //要改成第n題
        this.question2.scale.x = 0.2;
        this.question2.scale.y = 0.215;

        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q4_2o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_4_2, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q4_2o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_4_2, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q4_2o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_4_2, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q4_2o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_4_2, this);
    },
    clickOption1_4_2: function() {
        this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r4_2o1');
        this.r1.scale.x = 0.477;
        this.r1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_4_2 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 800 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption2_4_2: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+61, 'w4_2o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_2 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_2, this);
        player1_answered = true;
    },
    clickOption3_4_2: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+117, 'w4_2o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_2 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_2, this);
        player1_answered = true;
    },
    clickOption4_4_2: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'w4_2o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_2 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_2, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_4_2, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_4_2, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_4_2, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_4_2, '#33FFC6');
    }
};
var l4_3 = {
    preload: function() {
        game.load.image('player1_4_3', 'assets/l4_p1.jpg');
        game.load.image('player2_4_3', 'assets/l4_p2.jpg');
        game.load.image('bar_4_3', 'assets/bar.png');
        game.load.image('option_4_3', 'assets/option.png');
        game.load.image('q4_3o1', 'assets/q4_3o1.png'); //unique to each round
        game.load.image('q4_3o2', 'assets/q4_3o2.png'); //unique to each round
        game.load.image('q4_3o3', 'assets/q4_3o3.png'); //unique to each round
        game.load.image('q4_3o4', 'assets/q4_3o4.png'); //unique to each round
        game.load.image('gaming_4_3', 'assets/game_situation.jpg');
        game.load.image('q3', 'assets/question_3.png'); //unique to each round
        game.load.image('firstq', 'assets/l4_background.jpg'); //background
        game.load.image('question3', 'assets/4_3.png'); //unique to each round
        game.load.image('w4_3o1', 'assets/w4_3o1.png'); //unique to each round
        game.load.image('r4_3o2', 'assets/r4_3o2.png'); //unique to each round
        game.load.image('w4_3o3', 'assets/w4_3o3.png'); //unique to each round
        game.load.image('w4_3o4', 'assets/w4_3o4.png'); //unique to each round
        game.load.image('r4_3o2r', 'assets/r4_3o2r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_4_3 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_4_3');
        this.player1_4_3.scale.x = 0.083;
        this.player1_4_3.scale.y = 0.162;

        this.player2_4_3 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_4_3');
        this.player2_4_3.scale.x = 0.083;
        this.player2_4_3.scale.y = 0.162;

        this.correct_bar_1_p1_4_3 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 800 * 202));
        this.correct_bar_1_p2_4_3 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 800 * 202));

        this.q3 = game.add.sprite(game.width/2 - 104, game.height/2 - 127, 'q3'); //don't forget to modify!!!
        this.q3.scale.x = 0.538;
        this.q3.scale.y = 0.55;

        this.bar1_4_3 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_4_3');
        this.bar1_4_3.scale.x = 0.3;
        this.bar1_4_3.scale.y = 0.415;

        this.bar2_4_3 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_4_3');
        this.bar2_4_3.scale.x = 0.3;
        this.bar2_4_3.scale.y = 0.415;

        this.timeInSeconds_OP_4_3 = 120;
        this.timer_OP_4_3 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_4_3--;
        var minutes_OP_4_3 = Math.floor(this.timeInSeconds_OP_4_3 / 10);
        var seconds_OP_4_3 = this.timeInSeconds_OP_4_3 - (minutes_OP_4_3 * 10);
    
        if(seconds_OP_4_3 == 8) {
            this.show_OP();
            this.q3.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_4_3);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_4_3--;
        var minutes_4_3 = Math.floor(this.timeInSeconds_4_3 / 10);
        var seconds_4_3 = this.timeInSeconds_4_3 - (minutes_4_3 * 10);
        var timeString_4_3 = this.addZeros(seconds_4_3);
        this.timeText_4_3.text = timeString_4_3;
    
        if(seconds_4_3 == 5 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_4_3 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 800 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_4_3, this);
            player2_answered = true;
        }

        if (seconds_4_3 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_4_3 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r4_3o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_4_3, this);
            }
            else {
                this.r2 = game.add.sprite(game.width/2-102, game.height/2+63, 'r4_3o2r');
                this.r2.scale.x = 0.477;
                this.r2.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_4_3r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_4_3r() {
        game.time.events.remove(this.timer_4_3);
        this.timeText_4_3.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l4_4');
    },
    destroy_rectangle_p1_nextLevel_4_3() {
        this.wrong_bar_1_p1_4_3.width = 0;
        this.wrong_bar_1_p1_4_3.length = 0;
        game.time.events.remove(this.timer_4_3);
        this.timeText_4_3.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('l4_4');
    },
    destroy_rectangle_p1_4_3() {
        this.wrong_bar_1_p1_4_3.width = 0;
        this.wrong_bar_1_p1_4_3.length = 0;
    },
    destroy_rectangle_p2_4_3() {
        this.wrong_bar_1_p2_4_3.width = 0;
        this.wrong_bar_1_p2_4_3.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_4_3 = 120;
        this.timeText_4_3 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_4_3.anchor.set(0.5, 0.5);
        this.timer_4_3 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question3 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question3'); //要改成第n題
        this.question3.scale.x = 0.2;
        this.question3.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q4_3o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_4_3, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q4_3o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_4_3, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q4_3o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_4_3, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q4_3o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_4_3, this);
    },
    clickOption1_4_3: function() {
        this.w1 = game.add.sprite(game.width/2-102, game.height/2+5, 'w4_3o1');
        this.w1.scale.x = 0.477;
        this.w1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_3 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_3, this);
        player1_answered = true;
    },
    clickOption2_4_3: function() {
        this.r2 = game.add.sprite(game.width/2-102, game.height/2+61, 'r4_3o2');
        this.r2.scale.x = 0.477;
        this.r2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.correct_bar_1_p1_4_3 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 800 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption3_4_3: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+117, 'w4_3o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_3 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_3, this);
        player1_answered = true;
    },
    clickOption4_4_3: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'w4_3o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_3 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_3, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_4_3, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_4_3, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_4_3, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_4_3, '#33FFC6');
    }
};
var l4_4 = {
    preload: function() {
        game.load.image('player1_4_4', 'assets/l4_p1.jpg');
        game.load.image('player2_4_4', 'assets/l4_p2.jpg');
        game.load.image('bar_4_4', 'assets/bar.png');
        game.load.image('option_4_4', 'assets/option.png');
        game.load.image('q4_4o1', 'assets/q4_4o1.png'); //unique to each round
        game.load.image('q4_4o2', 'assets/q4_4o2.png'); //unique to each round
        game.load.image('q4_4o3', 'assets/q4_4o3.png'); //unique to each round
        game.load.image('q4_4o4', 'assets/q4_4o4.png'); //unique to each round
        game.load.image('gaming_4_4', 'assets/game_situation.jpg');
        game.load.image('q4', 'assets/last_question.png'); //unique to each round
        game.load.image('firstq', 'assets/l4_background.jpg'); //background
        game.load.image('question4', 'assets/4_4.png'); //unique to each round
        game.load.image('r4_4o1', 'assets/r4_4o1.png'); //unique to each round
        game.load.image('w4_4o2', 'assets/w4_4o2.png'); //unique to each round
        game.load.image('w4_4o3', 'assets/w4_4o3.png'); //unique to each round
        game.load.image('w4_4o4', 'assets/w4_4o4.png'); //unique to each round
        game.load.image('r4_4o1r', 'assets/r4_4o1r.png'); //unique to each round
    },
    create: function() {
        game.stage.backgroundColor = '#3498db';
        game.add.image(0, 0, 'firstq'); 
        this.background = game.add.tileSprite(0, 0, 600, 600, 'firstq');

        this.player1_4_4 = game.add.sprite(game.width/2-142.8, game.height/2-263.5, 'player1_4_4');
        this.player1_4_4.scale.x = 0.083;
        this.player1_4_4.scale.y = 0.162;

        this.player2_4_4 = game.add.sprite(game.width/2+70, game.height/2-263.5, 'player2_4_4');
        this.player2_4_4.scale.x = 0.083;
        this.player2_4_4.scale.y = 0.162;

        this.correct_bar_1_p1_4_4 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score) / 800 * 202));
        this.correct_bar_1_p2_4_4 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 800 * 202));

        this.q4 = game.add.sprite(game.width/2 - 125, game.height/2 - 127, 'q4'); //don't forget to modify!!!
        this.q4.scale.x = 0.538;
        this.q4.scale.y = 0.55;

        this.bar1_4_4 = game.add.sprite(game.width/2-145.5, game.height/2+6, 'bar_4_4');
        this.bar1_4_4.scale.x = 0.3;
        this.bar1_4_4.scale.y = 0.415;

        this.bar2_4_4 = game.add.sprite(game.width/2+125, game.height/2+6, 'bar_4_4');
        this.bar2_4_4.scale.x = 0.3;
        this.bar2_4_4.scale.y = 0.415;

        this.timeInSeconds_OP_4_4 = 120;
        this.timer_OP_4_4 = this.game.time.events.loop(Phaser.Timer.SECOND, this.countdownToShow, this);

    },
    countdownToShow: function() {
        this.timeInSeconds_OP_4_4--;
        var minutes_OP_4_4 = Math.floor(this.timeInSeconds_OP_4_4 / 10);
        var seconds_OP_4_4 = this.timeInSeconds_OP_4_4 - (minutes_OP_4_4 * 10);
    
        if(seconds_OP_4_4 == 8) {
            this.show_OP();
            this.q4.kill(); //don't forget to modify!!!!!
            game.time.events.remove(this.timer_OP_4_4);
        }
    },
    updateTimer: function() {
        this.timeInSeconds_4_4--;
        var minutes_4_4 = Math.floor(this.timeInSeconds_4_4 / 10);
        var seconds_4_4 = this.timeInSeconds_4_4 - (minutes_4_4 * 10);
        var timeString_4_4 = this.addZeros(seconds_4_4);
        this.timeText_4_4.text = timeString_4_4;
    
        if(seconds_4_4 == 9 && !destroyed) {
            player2_score += 200;
            this.correct_bar_1_p2_4_4 = new Phaser.Rectangle(285, 490, 10, -((player2_score) / 800 * 202));
            //this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p2_4_4, this);
            player2_answered = true;
        }

        if (seconds_4_4 == 0) {
            destroyed = true;
            if(!player1_answered) {
                this.wrong_bar_1_p1_4_4 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
                this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r4_4o1r');
                this.r1.scale.x = 0.477;
                this.r1.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_4_4, this);
            }
            else {
                this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r4_4o1r');
                this.r1.scale.x = 0.477;
                this.r1.scale.y = 0.455;
                this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_nextLevel_4_4r, this);
            }
        }
    },
    destroy_rectangle_p1_nextLevel_4_4r() {
        game.time.events.remove(this.timer_4_4);
        this.timeText_4_4.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('round_over');
    },
    destroy_rectangle_p1_nextLevel_4_4() {
        this.wrong_bar_1_p1_4_4.width = 0;
        this.wrong_bar_1_p1_4_4.length = 0;
        game.time.events.remove(this.timer_4_4);
        this.timeText_4_4.destroy();
        destroyed = false;
        player1_answered = false;
        player2_answered = false;
        game.state.start('round_over');
    },
    destroy_rectangle_p1_4_4() {
        this.wrong_bar_1_p1_4_4.width = 0;
        this.wrong_bar_1_p1_4_4.length = 0;
    },
    destroy_rectangle_p2_4_4() {
        this.wrong_bar_1_p2_4_4.width = 0;
        this.wrong_bar_1_p2_4_4.length = 0;
    },
    addZeros: function(num) {
        return num;
    },
    show_OP: function() {
        this.timeInSeconds_4_4 = 120;
        this.timeText_4_4 = this.game.add.text(155, 50, "10",{font: '40px Arial', fill: 
        '#FFFFFF', align: 'center'});
        this.timeText_4_4.anchor.set(0.5, 0.5);
        this.timer_4_4 = this.game.time.events.loop(Phaser.Timer.SECOND, this.updateTimer, this);

        this.question4 = game.add.sprite(game.width/2-120, game.height/2 - 100, 'question4'); //要改成第n題
        this.question4.scale.x = 0.2;
        this.question4.scale.y = 0.215;

        //don't forget to modify options!!!!!!!!
        this.option1 = game.add.sprite(game.width/2-102, game.height/2+7, 'q4_4o1');
        this.option1.scale.x = 0.453;
        this.option1.scale.y = 0.33;
        this.option1.inputEnabled = true;
        this.option1.input.useHandCursor = true;
        this.option1.events.onInputDown.add(this.clickOption1_4_4, this);

        this.option2 = game.add.sprite(game.width/2-102, game.height/2+63, 'q4_4o2');
        this.option2.scale.x = 0.453;
        this.option2.scale.y = 0.33;
        this.option2.inputEnabled = true;
        this.option2.input.useHandCursor = true;
        this.option2.events.onInputDown.add(this.clickOption2_4_4, this);

        this.option3 = game.add.sprite(game.width/2-102, game.height/2+119, 'q4_4o3');
        this.option3.scale.x = 0.453;
        this.option3.scale.y = 0.33;
        this.option3.inputEnabled = true;
        this.option3.input.useHandCursor = true;
        this.option3.events.onInputDown.add(this.clickOption3_4_4, this);

        this.option4 = game.add.sprite(game.width/2-102, game.height/2+175.5, 'q4_4o4');
        this.option4.scale.x = 0.453;
        this.option4.scale.y = 0.33;
        this.option4.inputEnabled = true;
        this.option4.input.useHandCursor = true;
        this.option4.events.onInputDown.add(this.clickOption4_4_4, this);
    },
    clickOption1_4_4: function() {
        this.r1 = game.add.sprite(game.width/2-102, game.height/2+5, 'r4_4o1');
        this.r1.scale.x = 0.477;
        this.r1.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.correct_bar_1_p1_4_4 = new Phaser.Rectangle(14.5, 490, 10, -((player1_score + 200) / 800 * 202));
        player1_score += 200;
        player1_answered = true;
    },
    clickOption2_4_4: function() {
        this.w2 = game.add.sprite(game.width/2-102, game.height/2+61, 'w4_4o2');
        this.w2.scale.x = 0.477;
        this.w2.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_4 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_4, this);
        player1_answered = true;
    },
    clickOption3_4_4: function() {
        this.w3 = game.add.sprite(game.width/2-102, game.height/2+117, 'w4_4o3');
        this.w3.scale.x = 0.477;
        this.w3.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option2.inputEnabled = false;
        this.option2.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_4 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_4, this);
        player1_answered = true;
    },
    clickOption4_4_4: function() {
        this.w4 = game.add.sprite(game.width/2-102, game.height/2+173.5, 'w4_4o4');
        this.w4.scale.x = 0.477;
        this.w4.scale.y = 0.455;
        this.option1.inputEnabled = false;
        this.option1.input.useHandCursor = false;
        this.option3.inputEnabled = false;
        this.option3.input.useHandCursor = false;
        this.option4.inputEnabled = false;
        this.option4.input.useHandCursor = false;
        this.wrong_bar_1_p1_4_4 = new Phaser.Rectangle(14.5, 288, 10, (800 - player1_score) / 800 * 202);
        this.game.time.events.add(Phaser.Timer.SECOND * 1.2, this.destroy_rectangle_p1_4_4, this);
        player1_answered = true;
    },
    render: function() {
        game.debug.geom(this.wrong_bar_1_p2_4_4, '#FF0000');
        game.debug.geom(this.wrong_bar_1_p1_4_4, '#FF0000');
        game.debug.geom(this.correct_bar_1_p1_4_4, '#33FFC6');
        game.debug.geom(this.correct_bar_1_p2_4_4, '#33FFC6');
    }
};



var game = new Phaser.Game(309, 550, Phaser.AUTO, 'canvas');
game.state.add('menu', menuState);
game.state.add('lobby', lobby);
game.state.add('round_over', round_over);
game.state.add('l1_1', l1_1);
game.state.add('l1_2', l1_2);
game.state.add('l1_3', l1_3);
game.state.add('l1_4', l1_4);
game.state.add('l1_5', l1_5);
game.state.add('l2_1', l2_1);
game.state.add('l2_2', l2_2);
game.state.add('l2_3', l2_3);
game.state.add('l2_4', l2_4);
game.state.add('l2_5', l2_5);
game.state.add('l3_1', l3_1);
game.state.add('l3_2', l3_2);
game.state.add('l3_3', l3_3);
game.state.add('l3_4', l3_4);
game.state.add('l3_5', l3_5);
game.state.add('l3_6', l3_6);
game.state.add('l4_1', l4_1);
game.state.add('l4_2', l4_2);
game.state.add('l4_3', l4_3);
game.state.add('l4_4', l4_4);
//game.state.start('l4_3');
game.state.start('menu');